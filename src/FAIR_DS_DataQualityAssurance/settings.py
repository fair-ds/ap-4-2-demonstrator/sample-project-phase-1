from dataclasses import dataclass
import logging
from pathlib import Path
import json
import os

SETTINGS_FILE = "settings.json"

# Set our defaults
DEFAULT = {
    "project_name": "My Project",
    "file_delimiter": ',',
    "project_folder": ".",
    "input": {
        "data_folder": "data",
        "schema_folder": "schemas",
        "schemas_file": "schemas.json"
        },
    "output": {
        "out_folder": "out",
        "log_folder": "logs",
        "package_file": "package.json"
    },
    "geodata": {},
    # EXAMPLE GEODATA Field
    # "geodata": {
    #     "data/my_file.csv": {
    #         "latitude": "latitude_column_name",
    #         "longitude": "longitude_column_name"
    #     }
    # }
    "crs": "EPSG:4326",
    "shape_file": "naturalearth_lowres",
    "shape_file_column": "name"
}

logger = logging.getLogger(__name__)


# From https://stackoverflow.com/a/23689767
class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]

    def clear(cls):
        try:
            del SingletonMeta._instances[cls]
        except KeyError:
            pass


@dataclass
class SettingsManager(metaclass=SingletonMeta):
    def __init__(self, filename=SETTINGS_FILE):
        if Path(filename).exists():
            logger.debug("loading settings file \"%s\"", filename)
            with open(filename, 'r') as f:
                file_data = json.load(f)
        else:
            logger.debug("no settings file present. using defaults")
            file_data = {}

        for key in DEFAULT.keys():
            value = file_data.get(key, DEFAULT[key])
            if isinstance(value, dict):
                value = dotdict(value)
            setattr(self, key, value)

    @property
    def data_folder(self):
        return (
            Path(self.project_folder) /
            Path(self.input.data_folder))

    @property
    def schema_folder(self):
        return (
            Path(self.project_folder) /
            Path(self.input.schema_folder))

    @property
    def schemas_file(self):
        return (
            Path(self.project_folder) /
            Path(self.schema_folder / self.input.schemas_file))

    @property
    def out_folder(self):
        return (
            Path(self.project_folder) /
            Path(self.output.out_folder))

    @property
    def log_folder(self):
        return (
            Path(self.project_folder) /
            Path(self.output.log_folder))

    @property
    def package_file(self):
        return (
            Path(self.project_folder) /
            Path(self.schema_folder / self.output.package_file))

    @property
    def infer_schemas_report(self):
        return (
            Path(self.project_folder) /
            Path(self.out_folder / "schema_report.xml"))

    @property
    def validation_report(self):
        return (
            Path(self.project_folder) /
            Path(self.out_folder / "validation_report.xml"))

    @property
    def data_integrity_report(self):
        return (
            Path(self.project_folder) /
            Path(self.out_folder / "data_report.xml"))

    @property
    def coscine_access_key(self):
        key = os.environ.get("COSCINE_S3_ACCESS_KEY_ID")
        if not key:
            logger.error("COSCINE_S3_ACCESS_KEY_ID not present")
        else:
            return key

    @property
    def coscine_secret_key(self):
        key = os.environ.get("COSCINE_S3_SECRET_ACCESS_KEY")
        if not key:
            logger.error("COSCINE_S3_SECRET_ACCESS_KEY not present")
        else:
            return key

    @property
    def coscine_bucket_name(self):
        key = os.environ.get("COSCINE_S3_BUCKET_NAME")
        if not key:
            logger.error("COSCINE_S3_BUCKET_NAME not present")
        else:
            return key

    @property
    def coscine_keys_provided(self):
        return (
            self.coscine_access_key and
            self.coscine_secret_key and
            self.coscine_bucket_name) is not None

    def create_folders(self):
        self.data_folder.mkdir(parents=True, exist_ok=True)
        self.schema_folder.mkdir(parents=True, exist_ok=True)
        self.out_folder.mkdir(parents=True, exist_ok=True)
        self.log_folder.mkdir(parents=True, exist_ok=True)

    def to_file(self, filename=SETTINGS_FILE):
        with open(filename, 'w') as f:
            json.dump(self.__dict__, f)
