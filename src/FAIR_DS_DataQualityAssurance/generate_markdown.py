import logging

FLAG_TO_EMOJI = {
    "SUCCESS": ":heavy_check_mark:",
    "INFO": ":grey_exclamation:",
    "WARNING": ":grey_question:",
    "ERROR": ":x:"
}

logger = logging.getLogger(__name__)


class Markdown:
    def __init__(self):
        self.doc = []

    def h1(self, text):
        self.doc.append(f"# {text}\n")

    def h2(self, text):
        self.doc.append(f"## {text}\n")

    def h3(self, text):
        self.doc.append(f"### {text}\n")

    def h4(self, text):
        self.doc.append(f"#### {text}\n")

    def h5(self, text):
        self.doc.append(f"##### {text}\n")

    def line(self):
        self.doc.append("")
        self.doc.append("---")
        self.doc.append("")

    def text(self, text):
        self.doc.append(f"{text}")

    def image(self, file, caption="", hover_text=""):
        self.doc.append(f"![{caption}]({file} {hover_text})")

    def add_list(self, my_list):
        for i in my_list:
            self._make_list_item(i)
        self.doc.append("")

    def _make_list_item(self, i, tabs=1):
        if type(i) == list:
            for j in i:
                self._make_list_item(j, tabs + 1)
        else:
            s = '\t'*(tabs-1) + f"- {i}"
            self.doc.append(s)

    def link(self, text, target):
        self.doc.append(self.get_link(text, target))

    def get_link(self, text, target):
        return f"[{text}]({target})"

    def code_block(self, text, syntax=""):
        self.doc.append(f"```{syntax}")
        self.text(text)
        self.doc.append("```")

    def to_file(self, filename):
        logger.info("writing markdown file to %s", filename)
        with open(filename, 'w', encoding="utf-8") as f:
            f.writelines([x+'\n' for x in self.doc])


def XML_to_mdList(xml):
    ts = []
    for testsuite in xml.testsuites:
        ts.append(testsuite.id)
        tc = []
        for testcase in testsuite.testcases:
            my_label = FLAG_TO_EMOJI[testcase.fail_type] + " " + testcase.name
            if testcase.fail_type == "SUCCESS":
                tc.append(my_label)
            else:
                tc.extend([my_label, [testcase.fail_text]])
        ts.append(tc)
    return ts
