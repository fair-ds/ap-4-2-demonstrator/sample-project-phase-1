import geopandas as gpd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from ..settings import SettingsManager


def load_base_shapefile():
    sm = SettingsManager()

    if sm.shape_file == "naturalearth_lowres":
        return gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
    else:
        raise NotImplementedError


def point_plot(
        gdf, filename=None, **kwargs):
    fig, ax = plt.subplots(figsize=(20, 20))
    load_base_shapefile().plot(ax=ax, color='lightgrey')
    gdf.plot(ax=ax, **kwargs)

    if filename:
        plt.savefig(filename)
    else:
        return plt

    plt.close("all")


def choropleth_plot(gdf, filename=None):
    sm = SettingsManager()

    base_map = load_base_shapefile()

    obs_by_shape = base_map.sjoin(
        gdf,
        how="inner",
        predicate='intersects').groupby(sm.shape_file_column).size()

    obs_by_shape.name = "N"
    obs_by_shape_df = base_map.merge(
        obs_by_shape, left_on=sm.shape_file_column, right_index=True)

    fig, ax = plt.subplots(figsize=(20, 20))
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    base_map.plot(ax=ax, color='lightgrey')
    obs_by_shape_df.plot(ax=ax, column='N', legend=True, cax=cax)

    if filename:
        plt.savefig(filename)
    else:
        return plt

    plt.close("all")
