"""Collect descriptive statistics about a given file.

Iterates over the columns of a given CSV file and calculates some simple
descriptive statistics for each. Any particuarly notable issues, like columns
entirely/mostly missing or having single values will be noted and added to a
report XML.

Args:
    filename: The path of the file we are going to validate. This file must
        exist.
    report_filename: The path of the xml report to produce. If this file does
        not already exist, it will be created.
"""

import argparse
from itertools import combinations
import json
import logging
from multiprocessing import Pool
from pathlib import Path
import time

import numpy as np
import pandas as pd
from scipy.stats import chi2_contingency

from JUnitXMLGen import TestSuites, TestSuite, TestCase

from .settings import SettingsManager
from .visualize import (
    create_categorical_data_plot,
    create_correlation_heatmap,
    create_correlation_plot,
    create_date_data_plot,
    create_missingness_plots,
    create_numerical_data_plot,
    get_interesting_correlations)
from .utils.file_io import get_output_filename
from .utils.load_data import load_csv_with_schema
from .utils.logging import setup_logging
from .utils.package import get_package_file, get_file_schema

logger = logging.getLogger(__name__)


def _parse_cli():
    parser = argparse.ArgumentParser(
        description="Describe/Visualize a file")

    parser.add_argument("--n_processes", type=int, default=8)
    parser.add_argument("--debug", action="store_true")

    return parser.parse_args()


def _check_desc(
        desc: dict, col: str, filename: str, is_numeric=False):

    xml = get_testsuite_xml()
    my_testsuite = TestSuite(
        f"check_desc|{filename}",
        f"Data Quality Check|{filename}")

    # Quick checks for questionable data
    if desc["Count"] == 0:
        my_testcase = TestCase(
            id=f"{col}_value_counts_0",
            name=f"{filename} | Column \"{col}\": No Values",
            fail_type="WARNING",
            fail_msg="No Values in column",
            fail_text=f"Column \"{col}\" contains no non-null data points")

        my_testsuite.add_testcase(my_testcase)

    elif desc["Percent Missing"] > 95:
        my_testcase = TestCase(
            id=f"{col}_highly_missing",
            name=f"{filename} | Column \"{col}\": >95% missing Values",
            fail_type="WARNING",
            fail_msg="Column is highly missing",
            fail_text=(
                f"Column \"{col}\" contains >95% missing values "
                f"({desc['Percent Missing']:2.2f})"))

        my_testsuite.add_testcase(my_testcase)

    if "Unique" in desc and desc["Unique"] == 1:
        my_testcase = TestCase(
            id=f"{col}_single_value",
            name=(f"{filename} | Column \"{col}\": "
                  "Only contains a single value"),
            fail_type="WARNING",
            fail_msg="Column contains only one value",
            fail_text=f"Column \"{col}\" contains only one value.")

        my_testsuite.add_testcase(my_testcase)

    # Checks for numerical data
    if is_numeric and desc["Standard Deviation"] == 0.0:
        my_testcase = TestCase(
            id=f"{col}_no_variation",
            name=f"{filename} | Column \"{col}\": No Variation",
            fail_type="WARNING",
            fail_msg="Column contains no variation",
            fail_text=f"Column \"{col}\" has no variation")

        my_testsuite.add_testcase(my_testcase)

    xml.add_testsuite(my_testsuite)

    xml.to_file(SettingsManager().data_integrity_report)


def get_testsuite_xml(
        filename=SettingsManager().data_integrity_report) -> TestSuites:
    return TestSuites(Path(__file__).name, filename)


def all_desc(col: pd.Series) -> dict:
    """Gets simple descriptive statistics for a given column

    Calls the .describe() function on the given column, then adds in a few
    more calculated values. The values returned depend on the column type
    (e.g. an object column will not have a "mean" field). These are determined
    by pandas' function.

    Args:
        col (pd.Series):
            The column to calculate statistics for.

    Returns:
        dict
            A dictionary containing relevant statistics for the column.
    """
    desc = col.describe(datetime_is_numeric=True).to_dict()

    # Replace the default keys with better formatted ones
    new_dict_keys = {
        "top": "Top",
        "freq": "Frequency",
        "count": "Count",
        "mean": "Mean",
        "std": "Standard Deviation",
        "min": "Minimum Value",
        "25%": "25th Percentile",
        "50%": "Median",
        "75%": "75th Percentile",
        "max": "Maximum Value",
        "unique": "Unique"
    }

    for old, new in new_dict_keys.items():
        if old in desc:
            desc[new] = desc[old]
            desc.pop(old)

    desc["Percent Unique"] = col.nunique()/len(col)
    desc["Missing Values"] = int(col.isnull().sum())  # why do I need int()?
    desc["Percent Missing"] = desc["Missing Values"]/len(col)*100

    # Don't bother taking samples unless the columns actually has some values
    if desc["Count"] > 0:
        sample = col.sample(min(len(col), 10), random_state=42).dropna()
        if len(sample) > 0:
            desc["Sample Values"] = [str(i) for i in sample.to_list()]
    return desc


def numeric_desc(col: pd.Series) -> dict:
    """Gets descriptive statistics for numeric columns

    Adds in to the dictionary returned by all_desc any additional values we
    want to calculate for a numeric column.

    Args:
        col (pd.Series):
            The column to calculate statistics for.

    Returns:
        dict
            A dictionary containing relevant statistics for the column.
    """
    desc = all_desc(col)
    desc["Median"] = col.median()
    desc["Unique"] = col.nunique()

    outliers = col[abs(col) > col.mean() + (3 * col.std())].tolist()
    if len(outliers) == 0:
        # Only create an outliers key if we have outliers
        desc["Outliers"] = outliers

    # Numpy int objects are't serializable, check if the sample has any and
    # convert any we find
    if "Sample Values" in desc:
        desc["Sample Values"] = [
            int(x) if type(x) in [np.int64, np.int32] else x
            for x
            in desc["Sample Values"]]

    return desc


def text_desc(col: pd.Series) -> dict:
    """Gets descriptive statistics for object columns

    Adds in to the dictionary returned by all_desc any additional values we
    want to calculate for an object column.

    Args:
        col (pd.Series):
            The column to calculate statistics for.

    Returns:
        dict
            A dictionary containing relevant statistics for the column.
    """
    desc = all_desc(col)

    # Sometimes text fields can just be long strings of descriptive text. We
    # should set a limit on how long these strings can get so the output isn't
    # cluttered
    MAX_LEN = 100

    def _trim_string(s, max_len):
        try:
            if len(s) <= max_len:
                return s
            else:
                return s[:max_len] + "..."
        except TypeError:
            if pd.isnull(s):
                return "NULL"
            else:
                return "?"

    if pd.notnull(desc["Top"]):
        desc["Top"] = _trim_string(desc["Top"], MAX_LEN)
    else:
        desc.pop("Top")

    if "Sample Values" in desc:
        desc["Sample Values"] = [
            _trim_string(s, MAX_LEN) for s in desc["Sample Values"]]

    return desc


def date_desc(col: pd.Series, date_format) -> dict:
    """Gets descriptive statistics for object columns

    Adds in to the dictionary returned by all_desc any additional values we
    want to calculate for an object column.

    Args:
        col (pd.Series):
            The column to calculate statistics for.

    Returns:
        dict
            A dictionary containing relevant statistics for the column.
    """
    desc = all_desc(col)

    # Convert these values back into strings so they are serializable
    # NOTE: Given https://specs.frictionlessdata.io/table-schema/#date we
    # can take the date_format "default" to be "YYYY-MM-DD"

    for k in ["Mean", "Minimum Value", "25th Percentile", "Median",
              "75th Percentile", "Maximum Value"]:
        try:
            desc[k] = desc[k].strftime("%Y-%m-%d")
        except Exception as e:
            logger.error("can't get %s for col \"%s\": %s", k, col.name, e)

    try:
        desc["Sample Values"] = [
            x.strftime("%Y-%m-%d") for x in desc["Sample Values"]]
    except Exception as e:
        logger.error(
            "can't get Sample Values for col \"%s\": %s", col.name, e)

    return desc


def perform_chi2_test(col1, col2):
    logger.debug("performing chi2 test on %s and %s", col1.name, col2.name)
    crosstab = pd.crosstab(col1, col2)
    c, p, dof, expected = chi2_contingency(crosstab)
    return p


def describe_file(filename, process_id):
    logger = logging.getLogger(f"PID#{process_id} {__name__}")
    log_file = (
        f"{Path(filename).stem}_describe_file_{int(time.time())}.log")
    setup_logging(logger, log_file)
    logger.info("describing %s", filename)

    # Load the data file
    # This function will set the index, but for this case we want to iterate
    # over all columns including the index, so reset it for now
    df = load_csv_with_schema(filename).reset_index()

    # Pull out some basic details about the file
    obs, cols = df.shape

    my_schema = get_file_schema(filename)

    # parse the schema to get a list of columns by file type
    fields_by_type = {
        t: []
        for t
        in set([field.type for field in my_schema.fields])}

    for field in my_schema.fields:
        fields_by_type[field.type].append(field.name)

    numeric_cols = (
        fields_by_type.get("integer", []) +
        fields_by_type.get("number", []))
    text_cols = (
        fields_by_type.get("string", []) +
        fields_by_type.get("any", []))
    date_cols = (
        fields_by_type.get("date", []) +
        fields_by_type.get("datetime", []))
    bool_cols = fields_by_type.get("boolean", [])

    # Initialize a data structure for us to put our descriptions into
    description = {
        "file": filename,
        "total observations": obs,
        "total variables": cols,
        "Numeric Variables": {c: {} for c in numeric_cols},
        "Text Variables": {c: {} for c in text_cols},
        "Date Variables": {c: {} for c in date_cols},
        "Boolean Variables": {c: {} for c in bool_cols}
    }

    # Descriptions by Column --------------------------------------------------

    # Create some simple statistics for each column in the dataset and store
    # them in a little dictionary
    for col in [f.name for f in my_schema.fields]:
        logger.debug("describing column %s", col)

        # Create a filename for the figures we create
        fig_filename = get_output_filename("out", filename, f"{col}_figs.png")
        logger.debug("saving plot to %s", fig_filename)

        try:
            if col in numeric_cols:
                desc = numeric_desc(df[col])
                description["Numeric Variables"][col] = desc

                create_numerical_data_plot(df[col], fig_filename)
            elif col in text_cols:
                desc = text_desc(df[col])
                description["Text Variables"][col] = desc

                create_categorical_data_plot(df[col], fig_filename)
            elif col in bool_cols:
                desc = text_desc(df[col].astype(object))
                description["Boolean Variables"][col] = desc

                create_categorical_data_plot(df[col], fig_filename)
            elif col in date_cols:
                desc = date_desc(df[col], my_schema.get_field(col).format)
                description["Date Variables"][col] = desc

                create_date_data_plot(df[col], fig_filename)
            else:
                raise Exception(f"What type is column \"{col}\"?")
        except (ValueError, TypeError):
            # If we hit a value error here, it's because the type specified in
            # the schema doesn't match the actual values. We're going to remove
            # that value from whatever list it's in and add it as text col
            if col in numeric_cols:
                numeric_cols.remove(col)
                description["Numeric Variables"].pop(col, None)
            if col in bool_cols:
                bool_cols.remove(col)
                description["Boolean Variables"].pop(col, None)
            if col in date_cols:
                date_cols.remove(col)
                description["Date Variables"].pop(col, None)

            text_cols.append(col)
            description["Text Variables"][col] = text_desc(df[col])
            create_categorical_data_plot(df[col], fig_filename)

        _check_desc(desc, col, filename, col in numeric_cols)

    xml = get_testsuite_xml()
    my_testsuite = TestSuite(
        f"check_desc|{filename}",
        f"Data Quality Check|{filename}")

    # If we don't have any comments on the data set, let's add a success
    # testcase
    if xml.total_tests == 0:
        my_testcase = TestCase(
            id=f"{filename}|qc_passed",
            name=f"{filename}: No Quality Issues Noted")
        my_testsuite.add_testcase(my_testcase)

    xml.add_testsuite(my_testsuite)

    xml.to_file(SettingsManager().data_integrity_report)

    # Descriptions by File ----------------------------------------------------

    # -- Correlations --

    # Pearson Correlations (linear)
    logger.debug("calculating pearson correlations...")
    corr = df.corr(method="pearson")
    out_file = get_output_filename(
        "out", filename, "corr_heatmap_pearson.png")
    create_correlation_heatmap(corr, filename, "Pearson", out_file)

    # Spearman Correlations (non linear)
    logger.debug("calculating spearman correlations...")
    corr = df.corr(method="spearman")
    out_file = get_output_filename(
        "out", filename, "corr_heatmap_spearman.png")
    create_correlation_heatmap(corr, filename, "Spearman", out_file)

    # Check our correlations for anything interesting
    logger.debug("checking for interesting numeric correlations...")
    for k, v in get_interesting_correlations(corr).items():
        col1, col2 = k
        out_file = get_output_filename(
            "out", filename, f"{col1}_{col2}_corr.png")
        create_correlation_plot(df[col1], df[col2], v, out_file)

    # Check our categorical columns for correlations with a chi2 test
    logger.debug("checking for categorical correlations...")
    for col1, col2 in combinations(description["Text Variables"].keys(), 2):

        logger.debug("comparing columns %s and %s", col1, col2)

        # Skip this test if either the column is highly unique
        if (description["Text Variables"][col1]["Percent Unique"] > 0.05 or
                description["Text Variables"][col2]["Percent Unique"] > 0.05):
            continue

        # Also skip this test if either of the columns is totally blank
        if (df[col1].notnull().sum() == 0 or df[col2].notnull().sum() == 0):
            continue

        # Also skip this test if either of the columns contains more than 10
        # values - it just gets too messy
        if (description["Text Variables"][col1]["Unique"] > 10 or
                description["Text Variables"][col2]["Unique"] > 10):
            continue

        p = perform_chi2_test(df[col1], df[col2])
        if p < 0.05:
            out_file = get_output_filename(
                "out", filename, f"{col1}_{col2}_corr.png")
            create_correlation_heatmap(
                pd.crosstab(df[col1], df[col2], normalize="all"),
                f"{col1} and {col2}",
                "Crosstab",
                out_file)

    # -- Missingness --
    logger.debug("creating missingness plots")
    out_file = get_output_filename("out", filename, "missingness.png")
    create_missingness_plots(df, out_file, filename)

    # save our descriptions to a file
    out_file = get_output_filename("out", filename, "desc.json")
    logger.info("saving descriptions to %s", out_file)
    with open(out_file, 'w') as f:
        json.dump(description, f)


def main(n_processes: int = 8):
    my_package = get_package_file()

    # Run describe_file for each file we see here
    work = [
        (
            resource.path,
            i
        )
        for i, resource
        in enumerate(my_package.resources)]

    with Pool(n_processes) as p:
        p.starmap(describe_file, work)


if __name__ == "__main__":
    # set up logging
    setup_logging(logger)
    logging.getLogger("matplotlib").setLevel(logging.WARNING)

    # Parse command line args
    args = _parse_cli()

    if args.debug:
        args.n_processes = 1

    main(args.n_processes)
