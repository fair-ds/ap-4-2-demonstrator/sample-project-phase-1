"""
infer_schemas.py

Variable names in all caps refer to variables set by either the user in a
settings.yml file or set to default values in settings.py.

This file will perform the following tasks:
    - Check for the existance of SCHEMAS_FILE, which is an optional file that
      can be provided by the user to indicate which files in DATA_FOLDER
      are assigned to which schemas.
        - This file will be checked for the following:
            - It is in the format expected?
            - Do the files indicated in the file exist?
            - Do the data files validate with their schemas?
    - Check the files in DATA_FOLDER
        - Are all of the files here accounted for in the schema file? (if one
          was provided)
        - If they are not all accounted for, which ones are remaining?
    - Create an XML report indicating if any of the above checks have failed,
      and which files have been assigned to which schemas.

Command Line Options:
    --debug: Set Logging to DEBUG and disables multithreading

Usage:
    Either through command line or by calling infer_schemas.main(). Relies on
    settings.py to know where folders are located and what various files
    should be named. These settings can be changed by including "settings.yml"
    in the working directory, with variables assigned as indicated in the
    document header of settings.py.
"""

import argparse
from collections import Counter
from itertools import chain
import json
import logging
from pathlib import Path
import time
from typing import Union

from JUnitXMLGen import TestSuites, TestSuite, TestCase

from .settings import SettingsManager
from .utils.logging import setup_logging
from .utils.package import get_package_file
from .utils.schema import infer_schema, get_schema_report, validate_schema

logger = logging.getLogger(__name__)


###############################################################################
# Script Specific Utilities
###############################################################################
def _parse_cli():
    parser = argparse.ArgumentParser(
        description="Check for and build Schema files")

    parser.add_argument("--debug", action="store_true")

    return parser.parse_args()


def get_all_data_files() -> list:
    """Scans DATA_FOLDER for all files ending in ".csv"

    Returns:
        list:
            A list of all of the data files located in the DATA_FOLDER
    """
    data_files = [
        f.as_posix()
        for f
        in SettingsManager().data_folder.glob("**/*.csv")]
    for f in data_files:
        logger.debug("found data file: %s", f)
    return data_files


def get_all_schema_files() -> list:
    """Scans SCHEMA_FOLDER for all files ending in ".json"

    TODO: Should we validate that each file is a true schema file?

    Returns:
        list:
            A list of all schema files found
    """
    schema_files = [
        f.as_posix()
        for f
        in SettingsManager().schema_folder.glob("**/*.json")]
    for f in schema_files:
        logger.debug("found schema file: %s", f)
    return schema_files


def get_data_files_in_schemas_json(schema_json):
    files = list(chain.from_iterable(
        [
            [Path(f).as_posix() for f in files]
            for _, files
            in schema_json.items()
        ]))
    for f in files:
        logger.debug("found referenced data file in schema: %s", f)
    return files


def get_testsuite_xml(
        filename=SettingsManager().infer_schemas_report) -> TestSuites:
    return TestSuites(Path(__file__).name, filename)

###############################################################################
# Process Steps
###############################################################################


def load_schemas_json(
        schemas_file: Path = SettingsManager().schemas_file) -> dict:
    """Load in data from the schemas json file

    Mostly error checking that the file is there and properly formatted. If
    something is found to be wrong, reports what it found to the xml test so
    the user will see it in the UI, as well as to the logger. Other than that,
    it really just tries to open the schemas file and load in the data as a
    dict.

    Returns:
        dict:
            The schemas file as a python dictionary.
    """
    schema_json = {}

    xml = get_testsuite_xml()

    # Create a new testsuite to report this step
    my_testsuite = TestSuite(
        "load_schemas_json|infer_schemas",
        "Load Schemas JSON|Schema Inference")

    # Check if a "schemas.json" file exists in the schemas folder, and if it
    # does, load the data. Otherwise, we'll just return the empty dict.
    if Path(schemas_file).exists():
        logger.debug("%s exists, loading...", schemas_file)
        with open(schemas_file, 'r') as f:
            try:
                # Try to load the JSON file we were given, and if it works,
                # add a JUnit testcase confirming that it worked.
                schema_json = json.load(f)
                my_testcase = TestCase(
                    id="found_schemas_json",
                    name=f"Found Schemas JSON {schemas_file}")
                my_testsuite.add_testcase(my_testcase)
            except json.JSONDecodeError as e:
                # If we get a json error when loading the file, send it to
                # the XML as a test case so it shows up in the report, as well
                # as to the logger
                my_testcase = TestCase(
                    id="invalid_schemas_json",
                    name=f"Invalid Schemas JSON File: {schemas_file}",
                    fail_type="ERROR",
                    fail_msg=(
                        f"Schemas JSON {schemas_file} is not in JSON format"),
                    fail_text=str(e))
                my_testsuite.add_testcase(my_testcase)
                logger.error(f"Invalid JSON Schemas file: \"{schemas_file}\"")

    else:
        msg = (f"No Schemas file is present ({schemas_file}). Any existing "
               "Schemas in the schemas folder will be automatically assigned")
        logger.warning(msg)
        my_testcase = TestCase(
            id="no_schemas_json",
            name=f"No Schemas JSON File present: {schemas_file}",
            fail_type="WARNING",
            fail_msg=msg,
            fail_text=msg)
        my_testsuite.add_testcase(my_testcase)

    # Add whatever happened here to the xml
    xml.add_testsuite(my_testsuite)

    xml.to_file(SettingsManager().infer_schemas_report)

    return schema_json


def validate_schemas_file(schemas_json: dict) -> dict:
    """Quick validation of schemas file

    Make sure we can locate the data files and schemas, and that no file is
    assigned to more than one schema.
    """
    xml = get_testsuite_xml()

    # Create a new testsuite to report this step
    my_testsuite = TestSuite(
        "validate_schemas_file|infer_schemas",
        "Validate Schemas File|Schema Inference")

    logger.debug("iterating over schemas_json...")
    # check that no file appears in more than one schema
    files = Counter()
    missing_schemas = []
    missing_files = []
    bad_files = []
    for schema_file, data_file_list in schemas_json.items():
        logger.debug("checking schema file %s...", schema_file)

        # check that the schema file exists
        if not Path(schema_file).exists():
            msg = (f"unable to locate schema file {schema_file} indicated in "
                   f"schemas file: " +
                   json.dumps(schemas_json))
            logger.warning(msg)
            my_testcase = TestCase(
                id=f"missing_schema_{schema_file}",
                name=f"Missing Schema File: {schema_file}",
                fail_type="WARNING",
                fail_msg=msg,
                fail_text=msg)
            my_testsuite.add_testcase(my_testcase)
            # add this schema to the list of schemas to be removed later
            missing_schemas.append(schema_file)
        else:
            for f in set(data_file_list):
                logger.debug("checking data file %s", f)
                # Checking for duplicates
                files.update((f,))

                # Check that the data file exists
                if not Path(f).exists():
                    msg = (f"unable to locate data file {f} specified "
                           f"under schema {schema_file}: " +
                           json.dumps(schemas_json))
                    logger.warning(msg)
                    my_testcase = TestCase(
                        id=f"missing_data_file_{f}",
                        name=f"Missing Data File: {f}",
                        fail_type="ERROR",
                        fail_msg=msg,
                        fail_text=msg)
                    my_testsuite.add_testcase(my_testcase)
                    # add this file to the list of files to be removed later
                    missing_files.append(f)
                elif not validate_schema(f, schema_file, strict=False):
                    # Check that the schema is valid when applied to this file
                    msg = (
                        f"data file \"{f}\" does not match schema "
                        f"\"{schema_file}\". "
                        "See validation errors for details.")
                    logger.error(msg)
                    my_testcase = TestCase(
                        id=f"failed_validation_{f}:{schema_file}",
                        name=(f"Failed to validate file \"{f}\" "
                              f"with \"{schema_file}\""),
                        fail_type="ERROR",
                        fail_msg=msg,
                        fail_text=msg)
                    my_testsuite.add_testcase(my_testcase)

                    # Get a schema report by checking the schema for errors,
                    # and then trying to validate the schema on the file
                    my_report = get_schema_report(f, schema_file)

                    # If we have either missing or incorrect labels in the
                    # report, then we can't use this schema to load the file
                    # and we should remove it from this schema and generate
                    # a new one
                    report_issues = Counter([t for t, msg in my_report])
                    if "column-mismatch" in report_issues:

                        msg = (f"Unable to apply schema file {schema_file} to "
                               f"data file {f}. Column Mismatch.")

                        my_testcase = TestCase(
                            id=f"invalid_schema_{schema_file}_on_{f}",
                            name=f"Invalid Schema Error: {schema_file} on {f}",
                            fail_type="ERROR",
                            fail_msg=msg,
                            fail_text=msg)
                        my_testsuite.add_testcase(my_testcase)

                        bad_files.append(f)
                    else:
                        # As long as the columns match, we're assuming the
                        # user intended for this schema to go with this data
                        # file, and so we should validate them together.
                        pass

    # Remove any missing schemas we found from the dict
    for missing_schema in missing_schemas:
        schemas_json.pop(missing_schema)

    # Remove any bad / missing files we found from the dict
    for file_to_remove in missing_files + bad_files:
        for schema in schemas_json.keys():
            if file_to_remove in schemas_json[schema]:
                schemas_json[schema].remove(file_to_remove)

    bad_files = [(f, count) for f, count in files.items() if count > 1]
    seen = []
    for bad_file, count in bad_files:
        msg = (f"multiple schemas per file: {bad_file} associated with "
               f"{count} schemas")
        logger.error(msg)
        my_testcase = TestCase(
            id=f"file_assigned_to_N_schemas_{bad_file}",
            name=f"File Assigned to Multiple Schemas: {schemas_json}",
            fail_type="ERROR",
            fail_msg=msg,
            fail_text=msg)
        my_testsuite.add_testcase(my_testcase)

        # iterate through the json and remove any instance of the bad file
        # after the first
        for _, files in schemas_json.items():
            if bad_file in files:
                if bad_file in seen:
                    files.remove(bad_file)
                else:
                    seen.append(bad_file)

    # Insert a success case if nothing went wrong here
    if len(my_testsuite.testcases) == 0:
        my_testcase = TestCase(
            id="schemas_file_passed_validation",
            name="No Issues in Schemas JSON File")
        my_testsuite.add_testcase(my_testcase)

    # Add whatever happened here to the xml
    xml.add_testsuite(my_testsuite)

    xml.to_file(SettingsManager().infer_schemas_report)

    return schemas_json


def guess_schema(filename: Union[str, Path]) -> Path:
    """Given a file, try and guess if an existing schema applies to it

    Will look at all of the schema files in the SCHEMA_FOLDER and try applying
    each in turn. If none of the schemas work for this file, we create a
    generic schema with the format "schema_X.json" and save it to that folder

    Args:
        filename (Union[str, Path]):
            The file to find a schema for

    Returns:
        Path:
            A path to the schema file that matches this file.
    """
    xml = get_testsuite_xml()

    # Create a new testsuite to report this step
    my_testsuite = TestSuite(
        "guess_schema|infer_schemas",
        "Identify Schema|Schema Inference")

    schema_filename = None
    for schema in get_all_schema_files():
        if validate_schema(filename, schema, strict=False):
            msg = f"found matching schema for {filename}: {schema}"
            logger.info(msg)
            my_testcase = TestCase(
                id=f"found_matching_schema_{filename}",
                name=f"Found Matching Schema for {filename}: {schema}")
            my_testsuite.add_testcase(my_testcase)
            schema_filename = Path(schema)

    if not schema_filename:
        msg = (f"No schema found that applies to {filename}.")
        logger.info(msg)
        my_testcase = TestCase(
            id=f"no_schemas_matching_{filename}",
            name=(
                f"No Schema Files in {SettingsManager().schema_folder} "
                f"match {filename}"),
            fail_type="WARNING",
            fail_msg=msg,
            fail_text=msg)
        my_testsuite.add_testcase(my_testcase)

        # We haven't found a matching schema, so we need to create one
        # The filename with be "schema_X", where X is N files with this name
        # format +1
        N = len([
            f
            for f
            in SettingsManager().schema_folder.glob("**/schema_*.json")])
        schema_filename = (
            SettingsManager().schema_folder / f"schema_{N+1}.json")

        msg = (f"Creating generic schema {schema_filename.as_posix()}")
        logger.info(msg)
        my_testcase = TestCase(
            id=f"creating_generic_schema_{schema_filename.as_posix()}",
            name=(f"Creating Generic Schema {schema_filename.as_posix()} "
                  f"for {filename}"),
            fail_type="WARNING",
            fail_msg=msg,
            fail_text=msg)
        my_testsuite.add_testcase(my_testcase)

        my_schema = infer_schema(filename)
        with open(schema_filename, 'w') as f:
            json.dump(my_schema, f)

    # Add whatever happened here to the xml
    xml.add_testsuite(my_testsuite)

    xml.to_file(SettingsManager().infer_schemas_report)

    return schema_filename


###############################################################################
# Main Function
###############################################################################
def main():
    """Main function for enclosing all operations in this script
    """
    # Ensure all folders we're using are created before we start
    sm = SettingsManager()
    sm.create_folders()

    # Load in any data we might need from the schemas file. (If no file exists,
    # we will just get back an empty dictionary)
    schema_json = load_schemas_json()

    # run our validation on that schemas file
    schema_json = validate_schemas_file(schema_json)

    # Check if there are any files in the data folder that aren't in the
    # schema file
    data_files = get_all_data_files()

    # Get all of the data files present in the schemas json
    files_in_schema = get_data_files_in_schemas_json(schema_json)

    my_testsuite = TestSuite(
        "schema_inference|infer_schemas",
        "Schema Inference|Schema Inference")

    files_not_in_schema = set(data_files) ^ set(files_in_schema)
    for file in files_not_in_schema:
        schema_file = guess_schema(file).as_posix()

        # Add this schema/file combo to the schemas file
        if schema_file not in schema_json:
            schema_json[schema_file] = []
        schema_json[schema_file].append(file)

    get_package_file(schemas=schema_json, force_build=True)
    my_testcase = TestCase(
        id="saving_package_json",
        name=f"Saving Package File to {SettingsManager().schemas_file}")
    my_testsuite.add_testcase(my_testcase)

    xml = get_testsuite_xml()
    xml.add_testsuite(my_testsuite)

    xml.to_file(SettingsManager().infer_schemas_report)


if __name__ == "__main__":
    args = _parse_cli()

    # set up logging
    setup_logging(logger, f"infer_schemas_{int(time.time())}.log")

    main()
