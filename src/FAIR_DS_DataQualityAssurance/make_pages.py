import argparse
import json
import logging
from multiprocessing import Pool
from pathlib import PurePath, Path
import pkg_resources
import re
import shutil
import time

import frictionless
from frictionless.exception import FrictionlessException

from JUnitXMLGen import TestSuites

from .generate_markdown import Markdown, FLAG_TO_EMOJI
from .settings import SettingsManager

from .utils.file_io import get_output_filename
from .utils.general import is_float, is_int
from .utils.logging import setup_logging
from .utils.package import (
    get_package_file, get_schema_filename_by_file, get_files_by_schema)
from .utils.schema import get_all_schemas

logger = logging.getLogger(__name__)


def _parse_cli():
    parser = argparse.ArgumentParser(
        description="Create Markdown files for Docsify")

    parser.add_argument("--data_folder")
    parser.add_argument(
        "--schemas_file", default=SettingsManager().schemas_file)
    parser.add_argument("--n_processes", type=int, default=8)
    parser.add_argument("--debug", action="store_true")

    return parser.parse_args()


def move_file(trunk_path, filename, dest_filename=None):
    if not dest_filename:
        dest_filename = filename
    source = get_output_filename("out", trunk_path, filename)
    target = get_output_filename(
        "out/pages", trunk_path, f"media/{dest_filename}")
    try:
        logger.debug("moving file from %s to %s", source, target)
        shutil.copy(source, target)
    except Exception as e:
        logger.warning("unable to move %s to %s (%s)", source, target, e)


def create_readme_md():
    def _get_status_list(my_testsuite):
        """helper for making the little status lists with num errors etc"""
        status_list = []
        for status in ["SUCCESS", "WARNING", "ERROR"]:
            count = len([
                tc for tc in my_testsuite.testcases if tc.fail_type == status])
            if count > 0:
                status_list.append(f"{status}: {count}")
        return ", ".join(status_list)

    def _make_status_list(md, xml):
        """helper for making markdown lists from the xml"""
        for ts in xml.testsuites:
            heading, _ = ts.name.split('|')
            status_list = _get_status_list(ts)
            item = [
                f"{heading} ({status_list})"]

            for tc in ts.testcases:
                desc = [f"{FLAG_TO_EMOJI[tc.fail_type]} {tc.name}"]
                if tc.fail_type == "SUCCESS":
                    item.append(desc)
                else:
                    item.append(desc + [tc.fail_msg.split('\n')])
            md.add_list(item)
        return md

    logger.info("creating main README page")

    md = Markdown()
    md.h1(SettingsManager().project_name)

    md.line()

    try:
        md.h1("Dataset Descriptions")

        # Create a visual of data files by schema
        md.h2("Data Files by Schema")
        md.code_block(json.dumps(get_files_by_schema(), indent=4))

        my_package = get_package_file()
        for resource in my_package.resources:
            link_target = get_output_filename(
                "", resource.path, "description.md", False).as_posix()
            md.h2(resource.path)
            md.add_list([md.get_link("Description", link_target)])
    except FrictionlessException as e:
        msg = ("Unable to load package file to create links on "
               f"landing page (FrictionlessException: {e})")
        logger.error(msg)
        md.text(msg)

    md.line()

    md.h1("Validation Steps")

    md.h2("Schema Validation")

    # Get a list of all schemas so we can preview them
    schemas_list = []
    for schema_file, schema_data in get_all_schemas().items():
        schema_page = create_schema_md(schema_file, schema_data)
        schemas_list.append(md.get_link(schema_file, schema_page))

    if len(schemas_list) > 0:
        md.add_list([
            "Avaliable Schemas:", schemas_list])
    else:
        md.text("WARNING: No schemas found!")

    md = _make_status_list(
        md,
        TestSuites(filename=SettingsManager().infer_schemas_report))

    md.h2("File Validation")
    md = _make_status_list(
        md,
        TestSuites(filename=SettingsManager().validation_report))

    md.h2("Data Checks")
    md = _make_status_list(
        md,
        TestSuites(filename=SettingsManager().data_integrity_report))

    md.to_file("out/pages/README.md")


def create_schema_md(schema_name, schema_data):
    clean_name = schema_name.replace(" ", "_")

    schema_page_path = get_output_filename(
        "out/pages", clean_name, "schema.md")

    if not schema_page_path.exists():
        logger.info("creating schema file: %s", schema_page_path)

        md = Markdown()

        md.h1("Schema File Details")
        md.h2(schema_name)

        md.code_block(schema_data.to_json(), "json")

        md.to_file(schema_page_path)

    return Path(clean_name) / "schema.md"


def create_description_md(filename, process_id):
    logger = logging.getLogger(f"PID#{process_id} {__name__}")
    setup_logging(logger)

    logger.info("Creating page for %s", filename)
    # build the path and create the folders
    page_file = get_output_filename("out/pages", filename, "description.md")

    # Load in the file description
    try:
        with open(get_output_filename("out", filename, "desc.json")) as f:
            file_desc = json.load(f)
    except FileNotFoundError as e:
        logger.error(
            ("Could not locate data description file %s. Verify that the "
             "description step sucessfully completed for this file."),
            filename)
        logger.error(e)
        return

    # Create the schema details page for this file

    # Initialize the markdown class
    md = Markdown()
    md.h1(f"File Details for {filename}")

    # Brief file details
    md.add_list([
        f"Total Observations: {file_desc['total observations']}",
        f"Total Variables: {file_desc['total variables']}",
        [
            f"{len(file_desc['Date Variables'])} date variables",
            f"{len(file_desc['Numeric Variables'])} numeric variables",
            f"{len(file_desc['Text Variables'])} string variables",
        ]
    ])

    # Helper for creating the markdown lists from the XML
    def _make_xml_list(xml):
        tcs = []
        for ts in xml.testsuites:
            if PurePath(ts.id) == PurePath(filename):
                for tc in ts.testcases:
                    my_label = FLAG_TO_EMOJI[tc.fail_type] + " " + tc.name
                    if tc.fail_type == "SUCCESS":
                        tcs.append(my_label)
                    else:
                        tcs.extend([my_label, [tc.fail_text]])

        if len(tcs) == 0:
            logger.error("%s doesn't appear in %s", filename, xml)
        return tcs

    md.h2("Schema Validation")
    schema_name = get_schema_filename_by_file(filename)
    with open(schema_name) as f:
        schema_data = frictionless.Schema(json.load(f))
    md.link(
        f"Assigned Schema: {schema_name}",
        create_schema_md(schema_name, schema_data))
    val_xml = TestSuites(filename="out/validation_report.xml")
    md.add_list(_make_xml_list(val_xml))

    md.h2("Data Checks")
    data_xml = TestSuites(filename="out/data_report.xml")
    data_checks_list = _make_xml_list(data_xml)
    if len(data_checks_list) > 0:
        md.add_list(data_checks_list)
    else:
        md.add_list(["No Data Issues Found"])

    # Missingness
    md.h2("Missingness")
    if get_output_filename("out", filename, "missingness.png").exists():
        move_file(filename, "missingness.png")
        md.image("media/missingness.png")
    else:
        md.add_list(["File contains no missing values"])

    md.h2("Variable Details")

    def format_var(s):
        if is_int(s):
            return f"{int(s):d}"
        elif is_float(s):
            return f"{float(s):.4f}"
        else:
            return s

    def _display_variable(col, details):
        md.h4(col)
        out = []
        for var, val in details.items():
            if isinstance(val, list):
                out.append(f"{var}:")
                out.append([format_var(i) for i in val])
            else:
                out.append(f"{var}: {format_var(val)}")
        md.add_list(out)
        move_file(
            filename,
            f"{col}_figs.png",
            f"{col.replace(' ', '_')}_figs.png")
        md.image(f"media/{col.replace(' ', '_')}_figs.png")

    var_types = [
        "Date Variables",
        "Numeric Variables",
        "Text Variables",
        "Boolean Variables",
        "Geographic Variables"]

    for var_type in var_types:
        if var_type in file_desc and len(file_desc[var_type]) > 0:
            md.h3(var_type)
            for col, details in file_desc[var_type].items():
                _display_variable(col, details)

    md.h2("Correlations")
    move_file(filename, "corr_heatmap_pearson.png")
    md.image("media/corr_heatmap_pearson.png")

    move_file(filename, "corr_heatmap_spearman.png")
    md.image("media/corr_heatmap_spearman.png")

    md.h2("Selected Variable Correlations")
    for f in get_output_filename("out", filename, "").glob("*_*_corr.png"):
        FILE_PATTERN = r'([A-z ]+)_([A-z ]+)_corr.png'
        col1, col2 = next(iter(re.findall(FILE_PATTERN, f.name)), (None, None))

        target_filename = f.name.replace(' ', '_')
        move_file(filename, f.name, target_filename)
        md.h3(f"{col1} and {col2}")
        md.image(f"media/{target_filename}")

    # Save to disk
    md.to_file(page_file)


def move_static_files():
    to_move = ["static/index.html", "static/_navbar.md"]
    for filename in to_move:
        assert pkg_resources.resource_exists(__name__, filename), \
            f"resource file \"{filename}\" is not present in the package"

        # ensure the destination directory exists
        Path(filename).parent.mkdir(parents=True, exist_ok=True)
        # get the absolute path to the resource
        source = pkg_resources.resource_filename(__name__, filename)

        # Make sure our destination folder exists
        Path("out/pages").mkdir(parents=True, exist_ok=True)

        # copy the file over
        destination = Path("out/pages") / Path(filename).name
        logger.debug("copying \"%s\" to \"%s\"", source, destination)
        shutil.copyfile(source, destination)


if __name__ == "__main__":
    # Set up logging
    setup_logging(logger, f"make_pages_{int(time.time())}.log")
    logging.getLogger("generate_testsuite_xml").setLevel(logging.WARNING)

    # Parse command line args
    args = _parse_cli()

    # Move static files to the local folder
    move_static_files()
    try:
        my_package = get_package_file()
        # Create the pages for each
        work = [
            (resource.path, i)
            for i, resource
            in enumerate(my_package.resources)]
        # If debugging, don't use multiple threads to make it easier to follow
        # exception tracebacks
        if args.debug:
            results = []
            for w in work:
                f, i = w
                create_description_md(f, i)
        else:
            # split up the work into n_processes different threads
            with Pool(args.n_processes) as p:
                p.starmap(create_description_md, work)
    except FrictionlessException as e:
        logger.error((f"Unable to load package file to create description "
                      f"pages (FrictionlessException: {e})"))

    create_readme_md()
