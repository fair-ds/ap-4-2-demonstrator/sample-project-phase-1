"""
validate.py

Variable names in all caps refer to variables set by either the user in a
settings.yml file or set to default values in settings.py.

This script will validate files indicated in SCHEMAS_FILE using the
frictionless standards and provide feedback in the form of an XML report.

Leverages the tableschema (https://pypi.org/project/tableschema/) library to
check that the format of a provided csv data file is in the correct format.
Requires the user to provide a schema file as "schemas/input-table.json" in
the format described in the link above.

Command Line Options:
    --n_processes (int, default=8):
        The maximum number of threads to create. With many files can increase
        the speed of this script.
    --debug: Sets logging to DEBUG and disables multithreading
"""

import argparse
import logging
from multiprocessing import Pool
from pathlib import Path
import time

from JUnitXMLGen import TestSuites, TestSuite, TestCase

from .settings import SettingsManager
from .utils.logging import setup_logging
from .utils.package import get_package_file
from .utils.schema import get_schema_report

logger = logging.getLogger(__name__)


def _parse_cli():
    parser = argparse.ArgumentParser(
        description="Validate file using TableSchema")

    parser.add_argument("--n_processes", type=int, default=8)
    parser.add_argument("--debug", action="store_true")

    return parser.parse_args()


def get_testsuite_xml(
        filename=SettingsManager().validation_report) -> TestSuites:
    return TestSuites(Path(__file__).name, filename)


def validate_file(filename, schema, process_id=None):
    # We can't write to a file, but we can at least dump some info to the
    # console about what's happening in a thread
    logger = logging.getLogger(f"PID#{process_id} {__name__}")
    log_file = f"{Path(filename).stem}_validation_{int(time.time())}.log"
    setup_logging(logger, log_file)

    logger.info("validating %s", filename)

    xml = get_testsuite_xml()
    my_testsuite = TestSuite(
        f"validate_file|{filename}",
        f"CSV Schema Validation|{filename}")

    # Load the file in with the given schema
    logger.debug("running validation code")

    my_report = get_schema_report(filename, schema)

    # We create a new testcase for each difference code type
    for code_type in set([code for code, msg in my_report]):
        logger.debug("adding testcase for code type %s", code_type)
        messages = [msg for code, msg in my_report if code == code_type]
        to_display = messages[:10]
        if len(messages) > 10:
            logger.debug(
                "got %d messages, trimming to first 10", len(messages))
            to_display.append(f"({len(messages)-10} Additional Messages)")

        case_id = f"{filename}|schema violation:{code_type}"
        case_name = f"{filename}: Schema Violation \"{code_type}\""

        my_testcase = TestCase(
            id=case_id,
            name=case_name,
            fail_type="ERROR",
            fail_msg='\n'.join(to_display),
            fail_text="")

        my_testsuite.add_testcase(my_testcase)

    # In the event we don't have any schema errors (num_test == 0), create
    # a blank testcase called SUCCESS.
    case_id = f"{filename}|schema_validated_{schema}"

    if my_testsuite.num_tests == 0:
        logger.info("schema validated!")

        my_testcase = TestCase(
            id=case_id,
            name=f"{filename}: File Passed Validation")

        my_testsuite.add_testcase(my_testcase)

    xml.add_testsuite(my_testsuite)

    xml.to_file(SettingsManager().validation_report)


def main(n_processes=8):
    # Get package date for this dataset
    my_package = get_package_file()

    # Run validate_file for each file we see here
    work = [
        (resource.path, resource.schema, i)
        for i, resource
        in enumerate(my_package.resources)]

    # If debugging, don't use multiple threads to make it easier to follow
    # exception tracebacks in the console
    if args.debug or n_processes == 1:
        results = []
        for w in work:
            file, schema, i = w
            results.append(validate_file(file, schema, i))
    else:
        # split up the work into n_processes different threads
        with Pool(args.n_processes) as p:
            results = p.starmap(validate_file, work)

    logger.info(
        "validation report saved to %s", SettingsManager().validation_report)


if __name__ == "__main__":
    # Parse any command line arguments
    args = _parse_cli()

    if args.debug:
        args.n_processes = 1

    # set up logging
    setup_logging(logger)

    main(args.n_processes)
