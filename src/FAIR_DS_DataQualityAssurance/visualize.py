"""Create visualizations for a given file

Args:
    filename: The path of the file we are going to validate. This file must
        exist.
"""

import logging

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

logger = logging.getLogger(__name__)

FIGURE_SIZE = (10, 5)


def get_subplots(num_plots):
    total_fig_size = FIGURE_SIZE[0], FIGURE_SIZE[1] * num_plots
    return plt.subplots(num_plots, 1, figsize=total_fig_size)


def create_numerical_data_plot(col: pd.Series, filename: str = None) -> None:
    """Create a pair of plots for the given numerical Series

    Populates the figure with a box plot and a histogram representing the data
    in this column.
    """
    # Bail if the column contains no data
    if col.notnull().sum() == 0:
        return

    green_diamond = dict(markerfacecolor='g', marker='D')

    fig, ax = plt.subplots(2, 1, figsize=(10, 10))

    fig.suptitle(f"Plots for Column {col.name}")

    # Box plot
    ax[0].boxplot(col.dropna(), vert=False, flierprops=green_diamond)
    ax[0].set_title(f"Box Plot for {col.name}")
    ax[0].set_ylabel(col.name)

    # Histogram
    ax[1].hist(col.dropna(), bins="fd")
    ax[1].set_title(f"Histogram for {col.name}")
    ax[1].set_ylabel("Count")

    # If provided with a filename, save the figure there, otherwise return
    # the plot
    if filename:
        plt.savefig(filename)
    else:
        return plt

    plt.close("all")


def create_categorical_data_plot(col: pd.Series, filename: str = None) -> None:
    """Create a pair of plots for the given categorical Series

    Populates the figure with a count plot and a pie plot representing the data
    in this column.
    """
    # Bail if the column contains no data
    if col.notnull().sum() == 0:
        return

    fig, ax = plt.subplots(2, 1, figsize=(10, 10))

    counts = col.value_counts()
    # If we get more than 6 values, create an "other" column
    if len(counts) > 6:
        other_col = pd.Series(counts.iloc[5:].sum(), ["Other Values"])
        counts = counts.iloc[:5].append(other_col)

    # Bar plot
    hbars = ax[0].barh(counts.index, counts.values)
    ax[0].invert_yaxis()
    ax[0].set_yticks(np.arange(len(counts.index)))
    ax[0].set_yticklabels(counts.index)
    ax[0].bar_label(hbars, fontsize=14, label_type="center", color='w')
    ax[0].set_title(f"Frequency Plot for variable \"{col.name}\"")

    # Pie plot
    ax[1].pie(
        counts.values,
        wedgeprops={"linewidth": 1, "edgecolor": "white"},
        labels=counts.index,
        autopct='%1.1f%%',
        startangle=90)
    ax[1].set_title(f"Pie Plot for variable \"{col.name}\"")

    # If provided with a filename, save the figure there, otherwise return
    # the plot
    if filename:
        # TODO: Not clear why we get an error here
        # maybe related: https://github.com/matplotlib/matplotlib/issues/14524
        try:
            plt.savefig(filename)
        except Exception as e:
            logger.error(e)
    else:
        return plt

    plt.close("all")


def create_date_data_plot(col: pd.Series, filename: str = None) -> None:
    fig, ax = get_subplots(num_plots=3)

    counts = col.value_counts()
    ax[0].bar(counts.index, counts.values)
    ax[0].set_title("Count of Records by Date")

    days_of_week = col.dt.dayofweek.map({
        0: "Sunday",
        1: "Monday",
        2: "Tuesday",
        3: "Wednesday",
        4: "Thursday",
        5: "Friday",
        6: "Saturday"
    }).value_counts()
    ax[1].pie(
        days_of_week,
        wedgeprops={"linewidth": 1, "edgecolor": "white"},
        labels=days_of_week.index,
        autopct='%1.1f%%',
        startangle=90)
    ax[1].set_title("Share of Records by Day of Week")

    day_of_year = col.dt.dayofyear.value_counts().sort_index()
    ax[2].bar(day_of_year.index, day_of_year.values)
    ax[2].set_xlim([1, 365])
    ax[2].set_title("Number of records by Day of Year")

    fig.tight_layout()
    fig.subplots_adjust(top=0.94)
    plt.suptitle(
        f"Plots For Column \"{col.name}\"", fontsize=14, fontweight="bold")

    # If provided with a filename, save the figure there, otherwise return
    # the plot
    if filename:
        plt.savefig(filename)
    else:
        return plt

    plt.close("all")


def create_correlation_heatmap(
        corr: np.array, subject: str,
        correlation_method: str, filename: str = None) -> None:
    """Create a heatmap showing the correlations in the provided 2D Array

    Populates the figure with a heatmap.
    """
    if subject != "":
        subject = f" for \"{subject}\""

    logger.debug("plotting %s correlations...", correlation_method)
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.imshow(corr, cmap="Blues", vmin=0, vmax=1)

    # Label the axes
    n_rows, n_cols = corr.shape
    ax.set_xticks(np.arange(n_cols))
    ax.set_yticks(np.arange(n_rows))
    ax.set_xticklabels(corr.columns)
    ax.set_yticklabels(corr.index)
    plt.setp(
        ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")

    # Add value annotations
    for i in range(n_rows):
        for j in range(n_cols):
            color = "w"
            if abs(corr.values[i, j]) < 0.25:
                # if the grid color is going to be very light, swap out the
                # font color for black
                color = "k"
            ax.text(
                j, i, f"{corr.values[i, j]:0.2f}",
                ha="center", va="center", color=color)

    ax.set_title(f"Heatmap of {correlation_method} Correlations{subject}")

    # If provided with a filename, save the figure there, otherwise return
    # the plot
    if filename:
        plt.savefig(filename)
    else:
        return plt

    plt.close("all")


def create_missingness_plots(df, filename=None, subject=""):
    """Create missingness plots for the provided dataframe
    """
    if df.isnull().sum().sum() == 0:
        return

    if subject != "":
        subject = f" for \"{subject}\""

    fig, ax = plt.subplots(2, 1, figsize=(10, 10))

    # -- Missingness Matrix --
    ax[0].imshow(
        df.reset_index(drop=True).notnull(),
        cmap='binary', aspect='auto', interpolation="none")

    ax[0].set_xticks(np.arange(len(df.columns)))
    ax[0].set_xticklabels(df.columns)
    plt.setp(
        ax[0].get_xticklabels(),
        rotation=90, ha="right", rotation_mode="anchor")
    ax[0].set_title(f"Missingness Matrix{subject}")

    # -- Nullity Correlation Heatmap --
    has_missing = df.isnull().sum()
    has_missing_cols = list(has_missing[has_missing > 0].index)
    corr = df[has_missing_cols].isnull().corr().fillna(1)

    ax[1].imshow(
        corr,
        cmap="coolwarm", aspect='auto', interpolation="none",
        vmin=-1, vmax=1)

    ax[1].set_xticks(np.arange(len(has_missing_cols)))
    ax[1].set_yticks(np.arange(len(has_missing_cols)))
    ax[1].set_xticklabels(has_missing_cols)
    ax[1].set_yticklabels(has_missing_cols)
    plt.setp(
        ax[1].get_xticklabels(),
        rotation=90, ha="right", rotation_mode="anchor")

    # Add value annotations
    for i in range(len(corr)):
        for j in range(len(corr)):
            color = "w"
            if abs(corr.values[i, j]) < 0.25:
                # if the grid color is going to be very light, swap out the
                # font color for black
                color = "k"
            ax[1].text(
                j, i, f"{corr.values[i, j]:0.2f}",
                ha="center", va="center", color=color)

    ax[1].set_title(f"Nullity Correlations Heatmap{subject}")

    # -- Final Touches --
    fig.tight_layout()
    fig.suptitle(f"Missingness Plots{subject}", fontsize=14)

    # Make space at the top of the plot for the suptitle
    plt.subplots_adjust(top=0.92)

    # If provided with a filename, save the figure there, otherwise return
    # the plot
    if filename:
        plt.savefig(filename)
    else:
        return plt

    plt.close("all")


def get_interesting_correlations(corr: np.array, min_r=0.5) -> dict:
    """Check the provided correlations for any particularly high values

    Any correlations that are larger than min_r will be added to a dictionary.
    Will ignore self correlations and mirrors.

    Returns:
        dict:
            The key is a tuple of column names and the value the r value.
    """
    plt_corrs = {}
    # Only keep values that are above the minimun r stat
    for k, v in corr[abs(corr) > min_r].to_dict().items():
        for i, j in v.items():
            # Ignore null values and self correlations
            if pd.notnull(j) and k != i:
                # Add this to the list as long as it's new
                if (k, i) not in plt_corrs and (i, k) not in plt_corrs:
                    plt_corrs[(k, i)] = j
    return plt_corrs


def create_correlation_plot(
        x: pd.Series, y: pd.Series, r: float, filename=None) -> None:
    """Given a pair of columns, creates a scatter plot with a regression line
    """
    logging.debug(f"making correlation plot for {x.name}/{y.name}")
    fig, ax = plt.subplots(figsize=(10, 5))

    # Create a scatter plot
    ax.plot(x.astype(float), y.astype(float), 'o')
    ax.set_xlabel(x.name)
    ax.set_ylabel(y.name)
    ax.set_title(f"Scatter plot for {x.name} and {y.name}")

    if filename:
        plt.savefig(filename)
    else:
        return plt

    plt.close("all")


def create_geo_plot(
        lat: pd.Series, lon: pd.Series,
        title: str, filename: str = None) -> None:
    fig, ax = plt.subplots(figsize=(10, 10))

    # Calculate the point density
    xy = np.vstack([lat.values, lon.values])
    z = gaussian_kde(xy)(xy)

    # Sort the points by density, so that the densest points are plotted last
    idx = z.argsort()
    x, y, z = lat.values[idx], lon.values[idx], z[idx]

    ax.scatter(x, y, c=z, s=10)
    ax.set_xlabel("Latitude")
    ax.set_ylabel("Longitude")
    ax.set_title(title)

    if filename:
        plt.savefig(filename)
    else:
        return plt

    plt.close("all")


def create_density_compare_plot(data, title, filename: str = None) -> None:
    fig, ax = plt.subplots(figsize=(10, 5))

    for label, values in data.items():
        ax.plot(values["x"], values["y"], label=label)

    ax.set_title(title)
    ax.set_ylabel("Density")
    ax.legend()

    if filename:
        plt.savefig(filename)
    else:
        return plt

    plt.close("all")
