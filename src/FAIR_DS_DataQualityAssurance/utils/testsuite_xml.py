from JUnitXMLGen import TestSuites, TestSuite


class add_testsuite:
    def __init__(self, name, filename, testsuite_name):
        self.filename = filename
        self.xml = TestSuites(name, self.filename)
        self.ts = TestSuite(
            testsuite_name,
            testsuite_name.lower().replace(' ', '_'))

    def __call__(self, f):

        def wrapped_f(*args, **kwargs):
            kwargs["ts"] = self.ts
            retval = f(*args, **kwargs)
            self.xml.add_testsuite(self.ts)
            self.xml.to_file(self.filename)
            return retval

        return wrapped_f
