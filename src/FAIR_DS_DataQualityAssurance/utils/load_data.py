import logging

import frictionless
import pandas as pd
import geopandas as gpd

from .package import get_file_schema
from ..settings import SettingsManager

logger = logging.getLogger(__name__)


def get_pandas_dtypes_from_schema(my_schema: frictionless.Schema):
    schema_to_pandas_dtypes = {
        "string": "object",
        "any": "object",
        "number": "float64",
        "integer": "Int64",
        "date": "datetime64",
        "datetime": "datetime64",
        "boolean": "bool",
        "geopoint": "object"}
    fields = {}
    for field in my_schema.fields:
        try:
            fields[field.name] = schema_to_pandas_dtypes[field.type]
        except KeyError:
            raise NotImplementedError(
                (f"schema type \"{field.type}\" is not implemented "
                 f"(field \"{field.name}\")"))
        except Exception as e:
            raise e

    return fields


def load_csv_with_schema(
        filename: str,
        schema_data=None,
        repair_schema=True) -> pd.DataFrame:
    """Load in a csv using the schema stored in SCHEMA_FOLDER.

    Args:
        filename (str):
            The filepath to the CSV file to load.

    Returns:
        pd.DataFrame:
            A dataframe created from the csv file.
    """
    sm = SettingsManager()
    if not schema_data:
        # Load the schema
        schema_data = get_file_schema(filename)

    if repair_schema:
        schema_data = fix_schema_types(filename, schema_data)

    # Read in the csv using pandas
    logger.info("loading %s...", filename)

    dtypes = get_pandas_dtypes_from_schema(schema_data)
    dtype_dict = {k: v for k, v in dtypes.items() if v != "datetime64"}
    dates_list = [k for k, v in dtypes.items() if v == "datetime64"]

    df = pd.read_csv(
        filename,
        sep=sm.file_delimiter,
        na_values=schema_data.missing_values,
        dtype=dtype_dict,
        parse_dates=dates_list,
        infer_datetime_format=True)

    if len(schema_data.primary_key) > 0:
        df.set_index(schema_data.primary_key, inplace=True)

    # Check if the user indicated that this file has geodata that we need to
    # create a new column out of
    if filename in sm.geodata.keys():
        for f, geo_cols in sm.geodata.items():
            if f == filename:
                assert "geo" not in df.columns, (
                    "data already contains a \"geo\" column")
                df["geo"] = df.apply(
                    lambda x:
                        (f"{x[geo_cols['latitude']]}, "
                         f"{x[geo_cols['longitude']]}"), axis=1)

    return df


def parse_geodata(
        col: pd.Series, fmt: str = "default", errors="coerce") -> pd.DataFrame:
    """Given a Column of geopoint data, parse it according to the format

    Args:
        col (pd.Series):
            A column of geopoint data

    """
    logger.debug("parsing geodata column %s with format %s", col.name, fmt)
    assert errors in ["raise", "coerce"]

    def parse_value(record, fmt):
        # Split up the values based on the format
        try:
            if fmt == "default":
                lon, lat = record.split(",")
                lon = lon.strip()
                lat = lat.strip()
            elif fmt == "array":
                lon, lat = record
            elif fmt == "object":
                lon, lat = record["lon"], record["lat"]

            lon = float(lon)
            lat = float(lat)
        except Exception as e:
            if errors == "coerce":
                return None, None
            else:
                raise TypeError(
                    f"Invalid datatype for geopoint column {col.name} ({e})")

        # Make sure the coordinates are valid
        try:
            assert abs(lon) < 180, (
                "lontitudinal coordinates must be between -180 and 180 "
                f"(recieved value: {lon})")
            assert abs(lat) < 90, (
                "latitudinal coordinates must be between -90 and 90 "
                f"(recieved value: {lat})")
        except AssertionError as e:
            if errors == "coerce":
                return None, None
            else:
                raise ValueError(f"invalid geopint data: {e}")

        return lon, lat

    col = col.apply(lambda x: parse_value(x, fmt))
    ret_val = pd.DataFrame(col.to_list(), columns=["lon", "lat"])
    logger.debug("sucessfully parsed %d records", len(ret_val))
    return gpd.points_from_xy(ret_val.lon, ret_val.lat)


def get_geopandas_df(filename, col="geo", **kwargs):
    df = load_csv_with_schema(filename, **kwargs)
    gdf = gpd.GeoDataFrame(
        df,
        geometry=parse_geodata(df[col], errors="coerce"),
        crs=SettingsManager().crs)
    return gdf


def fix_schema_types(filename, my_schema) -> frictionless.Schema:
    try:
        load_csv_with_schema(filename, my_schema, repair_schema=False)
    except ValueError:
        logger.debug(("Frictionless let us down. We're going to try and "
                      "figure out which column is the problem"))
        # frictionless isn#t great at inferring schemas sometimes.
        # Lets try to figure out which of these columns isn't actually
        # what it thinks - read them in one at a time as objects and try
        # to convert them to the appropriate data type. If it fails, we
        # know that one should be a string
        cols_to_check = {
            col: value_type
            for col, value_type
            in get_pandas_dtypes_from_schema(my_schema).items()
            if value_type != "object"}
        for col, value_type in cols_to_check.items():
            logger.debug("testing column %s - was a %s", col, value_type)
            try:
                pd.read_csv(filename, usecols=[col], dtype={col: value_type})
            except ValueError:
                logger.debug(
                    "col %s was a problem. Changing field type to string", col)
                for i, field in enumerate(my_schema.fields):
                    if field.name == col:
                        break
                my_schema.fields[i].type = "string"
            except TypeError:
                logger.debug("skipping %s: is a datetime", col)
                # We know this pattern fails on datetime data
                pass
        load_csv_with_schema(filename, my_schema, repair_schema=False)

    return my_schema
