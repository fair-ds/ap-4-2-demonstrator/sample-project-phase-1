from pathlib import Path
from JUnitXMLGen import TestSuites


def is_int(s):
    try:
        return int(s) == float(s)
    except Exception:
        return False


def is_float(s):
    try:
        float(s)
        return not is_int(s)
    except Exception:
        return False


def get_testsuite_xml(filename) -> TestSuites:
    return TestSuites(Path(__file__).name, filename)
