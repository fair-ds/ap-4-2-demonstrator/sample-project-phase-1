import json
import logging
from pathlib import Path
from typing import Union

import frictionless

from ..settings import SettingsManager

logger = logging.getLogger(__name__)


def _validate_and_log_errors(obj, f: str) -> None:
    """Tiny helper for logging and raising an exception

    Checks the object (either datapackage.Package or tableschema.Schema)
    to ensure that the .valid mathod is true. If not, prints all of the errors
    to the logger and raises a general exception.
    """
    report = frictionless.validate(obj)

    if len(report.errors) > 0:
        for e in report.errors:
            logger.error(e)
        raise Exception("Validation errors in %s", f)


def get_schema_filename_by_file(
        filename, package_file=SettingsManager().package_file) -> str:
    my_package = frictionless.Package(package_file)

    for resource in my_package.resources:
        if resource.path == filename:
            if "$ref" in resource.schema:
                return resource.schema["$ref"]
    return ""


def get_files_by_schema(package_file=SettingsManager().package_file):
    schemas = {}

    my_package = frictionless.Package(package_file)

    for resource in my_package.resources:
        if "$ref" in resource.schema:
            schema_name = resource.schema["$ref"]
            if schema_name not in schemas:
                schemas[schema_name] = []

            schemas[schema_name].append(resource.path)
    return schemas


def build_package_file(
        schemas: Union[str, Path, dict],
        use_refs: bool = True) -> frictionless.Package:
    """Given a collection of schemas, build a package file.

    Expects a minimum of two files:
        a "schemas" file: Should be a JSON file with a collection of key-value
        pairs, where each key is a schema file and each value a list of those
        files that follow that schema.

        Each schema file referred to in the schema file must exist, and should
        be a JSON file in valid frictionless format. See
        https://github.com/frictionlessdata/tableschema-py for more details.

        Args:
            schemas_file (str):
                The path and filename of the schemas file.
            use_refs (bool, default=True):
                If True, will build the package file using JSON $refs to local
                files. This format is not accepted by datapackage.Package, and
                so the refs must be replace with the full schema file before
                it will validate. Refs allow us to make changes only once
                in the referenced file if multiple files share the same schema
                and keeps the package file much shorter and readable.

        Returns:
            dict:
                A dictionary in a format similar to that used in
                datapackage.Package. If use_refs=False, it may be ingested
                immediatly. If use_refs=True (default), the JSON should be
                read in via load_package_json first.
    """
    package = {
        "profile": "tabular-data-package",
        "resources": []
    }

    base_resource_dict = {
        "path": None,
        "profile": "tabular-data-resource",
        "name": None,
        "title": None,
        "format": "csv",
        "mediatype": "text/csv",
        "encoding": "utf-8",
        "schema": None
    }

    if isinstance(schemas, str) or isinstance(schemas, Path):
        logger.debug("loading schema file %s", schemas)
        with open(schemas) as f:
            all_schemas = json.load(f)
    else:
        all_schemas = schemas

    for schema_file, files in all_schemas.items():
        # Check to be sure that this schema is valid
        # before we go incorporating it into our file
        my_schema = frictionless.Schema(schema_file)
        _validate_and_log_errors(my_schema, schema_file)

        for f in files:
            logger.debug("adding schema %s for file %s", schema_file, f)
            resource = base_resource_dict.copy()
            resource["path"] = f
            resource["name"] = Path(f).stem.lower()
            resource["title"] = Path(f).stem
            if use_refs:
                resource["schema"] = {"$ref": schema_file}
            else:
                resource["schema"] = json.loads(my_schema.to_json())

            package["resources"].append(resource)

    logger.info("sucessfully built package file")
    return frictionless.Package(package)


def load_package_json(package_file: str) -> frictionless.Package:
    """Load in a given package file and replace any JSON $refs

    JSON $refs allow us to reference a single file multiple times. The standard
    datapackage.Package dictionary will have a seperate schema for each and
    every file, which can be bloated and tedious to edit. This function will
    iterate over a given package file and replace all of the $ref key/value
    pairs with the contents of the referenced file. If no refs are found,
    the dictionary will just be returned.

    NOTE: There's probably a much more clever recursive solution to this
    problem, but since we know exactly where the refs will sit, this seems
    unnessecary and avoids using another import like jsonref.

    Args:
        package_file (str):
            The path and filename of the package file.

    Returns:
        frictionless.Package
    """
    logger.info("loading package file %s", package_file)
    my_package = frictionless.Package(package_file)

    # Fill out any $refs we have with the full schema
    for resource in my_package.resources:
        if "$ref" in resource.schema:
            resource.schema = frictionless.Schema(resource.schema["$ref"])

    _validate_and_log_errors(my_package, package_file)

    logger.info("sucessfully loaded package file from %s", package_file)
    return my_package


def get_package_file(
        package_file: Union[str, Path] = SettingsManager().package_file,
        schemas: Union[str, Path, dict] = SettingsManager().schemas_file,
        force_build=False) -> frictionless.Package:
    """Read in a package file, or create one if it does not exist.

    If the package file is not present, will call build_package_file and save
    the result to the path specified in package_file. If the file is present,
    it will load the file and check it for errors before returning the
    file loaded in as a datapackage.Package object.

    Args:
        package_file (str):
            The local file path of the package file. If one is not provided,
            the the default is assumed to be "package.json" in the
            SCHEMA_FOLDER path.

        schemas_file (str):
            Must Exist. The local file path of the schemas file. If one is not
            provided, the the default is assumed to be "schemas.json" in the
            SCHEMA_FOLDER path.

    Returns:
        frictionless.Package:
            The package_file loaded and validated as Package.

    """
    # let us pass in strings if we want
    if isinstance(package_file, str):
        package_file = Path(package_file)
    if isinstance(schemas, str):
        schemas = Path(schemas)

    if not package_file.exists() or force_build:
        # The package file doesn't exist or the user want's to build a new
        # one, so let's use the schema files to construct one
        with open(package_file, 'w') as f:
            json.dump(build_package_file(schemas), f, indent=4)

    # load in the data from our package file
    my_package = load_package_json(package_file)
    _validate_and_log_errors(my_package, package_file)

    return my_package


def get_file_schema(filename) -> frictionless.Schema:
    pkg = get_package_file()

    for resource in pkg.resources:
        if Path(resource.path) == Path(filename):
            return resource.schema

    err = (f"No file called {filename} could be "
           f"found in schemas file {SettingsManager().schemas_file}")
    logger.error(err)
    # This should never happen unless something in the schemas file is
    # incorrect. Hard failure if we ever get here.
    raise FileNotFoundError(err)
