import logging
import os
from pathlib import Path

logger = logging.getLogger(__name__)


def gather_files(root_dir: str) -> list:
    """Search the given directory for all csv files"""
    if not Path(root_dir).is_dir():
        raise FileNotFoundError('Directory is not valid!')
    return [file for file in Path(root_dir).glob('**/*.csv')]


def get_output_filename(
        root_folder: str,
        data_file: str,
        filename: str,
        create_folders=True) -> Path:
    """Creates a filepath given a folder and a pair of filenames.

    Used to create a standard way of outputting files from these scripts. We
    could just do this in the scripts, but this way if we decide to change the
    structure of the "out" folder we only have to change it here.

    Typically, "root_folder" is "out", and "data_file" is the relative path of
    the csv data file.

    Args:
        root_folder (str):
            The root folder to use. Typically "out"
        data_file (str):
            At the moment, we use the same structure as the files we find in
            the DATA_FOLDER. This is usually a relative path to a data file,
            from which we pull the parent path.
        filename (str):
            The filename to give this item.
        create_folders (bool, default=True):
            If True, will create all folders which do not currently exist in
            the filepath.
    """
    out = (Path(root_folder) /
           Path(data_file) /
           Path(filename))

    if create_folders:
        # Make these folders in case they don't exist
        os.makedirs(os.path.dirname(out), exist_ok=True)

    return out
