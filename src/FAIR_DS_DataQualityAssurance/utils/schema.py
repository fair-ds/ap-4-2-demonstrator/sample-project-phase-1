import difflib
import json
import logging
from pathlib import Path
from typing import Union

import frictionless
import pandas as pd

from ..settings import SettingsManager
from .load_data import fix_schema_types

logger = logging.getLogger(__name__)


def get_suggested_constraints(series: pd.Series, data_type: str) -> dict:
    """
    Guess some constraints that might apply to this series

    Args:
        series (pd.Series):
            The series to look at.
        data_type (str):
            The Data Type identified when frictionless looked at the file.

    Returns:
        dict:
            A dictionary of suggested constraints that canbe added into a
            frictionless schema JSON.
    """
    constraints = {}

    # is the column totally unique?
    is_unique = series.nunique() == len(series)
    if is_unique:
        constraints["unique"] = True

    # are there any missing values?
    no_missing = series.isnull().sum()
    if no_missing:
        constraints["required"] = True

    # If it's a numeric column, suggest a max/min value
    if (
            data_type == "number" or
            (
                data_type == "integer" and
                not is_unique
            )):
        constraints["minimum"] = int(series.min())
        constraints["maximum"] = int(series.max())

    # If it's a string column, could it be a categorical? If not, what's the
    # shortest/longest string we see?
    if data_type == "string":
        is_enum = series.nunique() < 10
        if is_enum:
            constraints["enum"] = sorted(list(series.unique()))
        else:
            constraints["minLength"] = int(series.apply(len).min())
            constraints["maxLength"] = int(series.apply(len).max())

    return constraints


def infer_schema(filename) -> frictionless.Schema:
    """
    Infer the schema of a given file.
    """
    logger.info("Inferring the schema for \"%s\"", filename)
    my_schema = frictionless.describe_schema(filename)
    my_schema = fix_schema_types(filename, my_schema)

    for i, field in enumerate(my_schema.fields.copy()):
        col, data_type = field.name, field.type
        series = pd.read_csv(filename, usecols=[col])[col]

        constraints = get_suggested_constraints(series, data_type)

        if len(constraints.keys()) > 0:
            my_schema.fields[i]["constraints"] = constraints

    return my_schema


def validate_schema(
        filename: Union[Path, str],
        schema: Union[frictionless.Schema, dict, str, Path],
        strict=True
        ) -> bool:
    """Validate a given schema on a given file

    Applies the schema to the file and checks to make sure it passes
    frictionless' validation in the report.

    Args:
        filename (Union[Path, str]):
            The Path or filename of the file to be validated
        schema (Union[frictionless.Schema, dict]):
            The schema to chec on the file

    Returns:
        bool
    """
    my_resource = frictionless.Resource(filename, schema=schema)

    if strict:
        skip_errors = []
    else:
        skip_errors = [
            "constraint-error",
            "type-error",
            "blank-row",
            "unique-error"]

    return frictionless.validate(my_resource, skip_errors=skip_errors).valid


def get_schema_report(
        filename: Union[Path, str],
        schema: Union[frictionless.Schema, dict, str, Path]) -> list[str]:

    # Load in the schema if we need to
    if isinstance(schema, dict):
        schema = frictionless.Schema(schema)
    elif (isinstance(schema, Path) or isinstance(schema, str)):
        with open(schema) as f:
            schema = frictionless.Schema(json.load(f))

    my_report = []

    # Check if the columns match up. If they don't we're going to get a
    # mismatched label, extra label, or missing label, which is fine, but then
    # we might get a shedload of type warkings cluttering things up. We're
    # going to short circut things here and make our own message if that's the
    # case
    data_file_cols = list(pd.read_csv(filename, nrows=0).columns)
    if len(set(data_file_cols) ^ set(schema.field_names)) != 0:
        logger.debug(
            "Identified a schema column mismatch for file %s", filename)
        return [(
            "column-mismatch",
            '/n'.join([
                s
                for s
                in difflib.context_diff(data_file_cols, schema.field_names)]))]

    my_resource = frictionless.Resource(filename, schema=schema)
    report = frictionless.validate(my_resource)

    if not report.valid:
        for item in report.tasks[0].errors:
            report_item = (
                item.code,
                f"{item.description} {item.message}")
            if report_item not in my_report:
                my_report.append(report_item)
                logger.debug("report item: %s", report_item)

    return my_report


def load_schema(name: str) -> frictionless.Schema:
    """Load a given schema file

    Assumes that schema files will be stored in the SCHEMA_FOLDER

    Args:
        name (str):
            The name of the schema file to load

    Returns:
        frictionless.Schema
    """
    filename = Path(name)
    if not filename.exists():
        raise FileNotFoundError(f"{filename} does not exist")
    return frictionless.Schema(filename)


def get_fields_of_type(my_schema, my_type):
    fields_by_type = {
        t: []
        for t
        in set([field.type for field in my_schema.fields])}

    for field in my_schema.fields:
        fields_by_type[field.type].append(field.name)

    if my_type == "numeric":
        return (
            fields_by_type.get("integer", []) +
            fields_by_type.get("number", []))
    elif my_type == "text":
        return (
            fields_by_type.get("string", []) +
            fields_by_type.get("any", []))
    elif my_type == "date":
        return fields_by_type.get("date", [])
    elif my_type == "bool":
        return fields_by_type.get("boolean", [])
    elif my_type == "geo":
        return fields_by_type.get("geopoint", [])
    else:
        raise Exception(f"Unrecognized type: {my_type}")


def get_all_schemas():
    schemas = {}

    for schema_file in Path(SettingsManager().schema_folder).glob("**/*.json"):
        schema = frictionless.Schema(schema_file)
        if frictionless.validate(schema).valid:
            schemas[schema_file.as_posix()] = schema

    # TODO: idea for later. scan the schemas folder and see if we missed any
    # with open(SCHEMAS_FILE) as f:
    #     schemas_data = json.load(f)

    # for schema_file in schemas_data.keys():
    #     if r.resolve() not in good_schemas:
    #         with open(schema_file) as f:
    #             bad_schemas[schema_file.as_posix()] = json.load()

    return schemas


# UNUSED

# def schema_similarity(
#         s1: frictionless.Schema, s2: frictionless.Schema) -> float:
#     """Calculate roughly how similar two schemas are

#     Compare two frictionless.Schema files and get a value indicating how
#     similar they are based on how many field names, field types, and
#     missing vales are shared/not. The "metric" is very rough, and really just
#     helps find two schemas that are almost identical but have a field or two
#     with mismatched types.

#     Args:
#         s1 (frictionless.Schema):
#             The first schema file to compare
#         s2 (frictionless.Schema):
#             The second schema file to compare

#     Returns:
#         float:
#             a value between 0 and 1 indicating how "similar" the schemas are
#     """
#     # weights, since some measures are more important
#     weights = {
#         "name": 0.75,
#         "type": 0.2,
#         "missing": 0.05}

#     s1_fields = set([f.name for f in s1.fields])
#     s2_fields = set([f.name for f in s2.fields])

#     # Set up some sets
#     extra_fields = s1_fields ^ s2_fields
#     common_fields = s1_fields & s2_fields
#     total_fields = len(s1_fields) + len(s2_fields)

#     # How many fields are in one but not the other?
#     field_name_score = len(extra_fields) / total_fields

#     # How many times does the field type not match?
#     type_mismatches = len(extra_fields)
#     for field in common_fields:
#         type_mismatches += int(
#             s1.get_field(field).type != s2.get_field(field).type)
#     type_score = type_mismatches / total_fields

#     # How do the missing values line up?
#     s1_missing = set(s1.missing_values)
#     s2_missing = set(s2.missing_values)

#     extra_missing = s1_missing ^ s2_missing
#     total_missing = len(s1_missing) + len(s2_missing)
#     missing_score = len(extra_missing) / total_missing

#     # Apply the weighting to get a final score
#     final_score = (
#         (field_name_score * weights["name"]) +
#         (type_score * weights["type"]) +
#         (missing_score * weights["missing"]))

#     if len(extra_fields) != 0:
#         return 0

#     # return the inverse, so that 1 == Identical
#     return 1 - final_score
