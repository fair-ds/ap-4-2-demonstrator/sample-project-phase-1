import logging
from pathlib import Path

from ..settings import SettingsManager

LOG_LEVEL = logging.DEBUG
LOG_FORMAT = logging.Formatter(
    "%(asctime)s -%(funcName)s - %(levelname)s - %(message)s")


def setup_logging(my_logger, file_logger=None):
    sm = SettingsManager()
    my_logger.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(LOG_LEVEL)
    ch.setFormatter(LOG_FORMAT)
    my_logger.addHandler(ch)

    if file_logger:
        # if the logging folder doesn't exist yet, make it here
        sm.log_folder.mkdir(parents=True, exist_ok=True)

        fh = logging.FileHandler(sm.log_folder / Path(file_logger))
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(LOG_FORMAT)
        my_logger.addHandler(fh)

    return my_logger
