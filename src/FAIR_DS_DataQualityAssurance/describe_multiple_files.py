import pandas as pd

from .visualize import create_missingness_plots

def missingness_across_multiple_files(
        files: list[str], schema_name: str, out_file: str = None) -> None:
    """Create a missingness plot from a collection of files, concated together

    Args:
        files (list):
            A list of files to be read in and included.
        schema_name (str):
            The name of the schema these files belong to.
        out_file (str, optional):
            The name of the image file to be created. If None, no file will
            be created.

    Returns:
        If out_file is None, returns the matplotlib.pyplot "plt" object,
        which contains the current plot. Otherwise, saves the plot to disk and
        returns None.
    """
    df = pd.concat([pd.read_csv(f).notnull().astype(int) for f in files])
    create_missingness_plots(
        df=df.replace({0: None}),
        filename=out_file,
        subject=f"Files with schema \"{schema_name}\"")
