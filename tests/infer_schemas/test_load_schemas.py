import json
from pathlib import Path

from src.FAIR_DS_DataQualityAssurance.settings import SettingsManager
from src.FAIR_DS_DataQualityAssurance.infer_schemas import (
    load_schemas_json, get_testsuite_xml)
from tests.setups import Setups

my_schemas = {
    "schemas/schema_1.json": [
        "data/file_1.csv",
        "data/file_2.csv"
    ],
    "schemas/schema_2.json": [
        "data/file_3.csv"
    ]
}

bad_schemas = """{
    schemas/schema_1.json:
        - data/file_1.csv
        - data/file_2.csv
    schemas/schema_2.json:
        - data/file_3.csv
}"""


class Test_LoadSchemas:
    """
    I will return an empty dict if the user provides no schemas file
    I will provide a dict and log to the testsuite if a file is provided
    I will return an empty dict if the user provides a malformed
    """

    """
    ===========================================================================
    Test Function Behaviour
    ===========================================================================
    """
    def test_good_schemas_file(self, setup_from_schema):
        # Setup
        Setups.Full.valid(setup_from_schema)

        with open(SettingsManager().schemas_file, 'w') as f:
            json.dump(my_schemas, f)

        # Test
        assert SettingsManager().schemas_file.exists()
        ret_val = load_schemas_json()
        assert ret_val == my_schemas

    def test_no_schemas_file_present(self):
        # If the SCHEMAS_FILE isn't present, then we should get a blank dict.
        assert not SettingsManager().schemas_file.exists()
        assert load_schemas_json() == {}

    def test_invalid_schemas_file(self):
        # Setup
        # create a non-JSON formatted file
        with open(SettingsManager().schemas_file, 'w') as f:
            f.write(bad_schemas)

        # Test
        # We should get a blank dictionary back, because that wasn't JSON
        assert SettingsManager().schemas_file.exists()
        assert load_schemas_json() == {}

    """
    ===========================================================================
    Test Messages written to the XML
    ===========================================================================
    """
    def test_message_found_schemas_file(self, setup_from_schema):
        # Setup
        setup_from_schema(my_schemas)

        with open(SettingsManager().schemas_file, 'w') as f:
            json.dump(my_schemas, f)

        # Test
        assert Path(SettingsManager().schemas_file).exists()
        load_schemas_json()

        ts = get_testsuite_xml().get_testsuite(
            "load_schemas_json|infer_schemas")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 1
        assert ts.get_testcase("found_schemas_json")

    def test_message_not_found_schemas_file(self, setup_from_schema):
        setup_from_schema(my_schemas)

        # Test
        assert not Path(SettingsManager().schemas_file).exists()
        load_schemas_json()

        ts = get_testsuite_xml().get_testsuite(
            "load_schemas_json|infer_schemas")
        assert ts.worst_message == "WARNING"
        assert ts.num_tests == 1
        assert ts.get_testcase("no_schemas_json")

    def test_message_invalid_schemas_file(self):
        # Setup
        # create a non-JSON formatted file
        with open(SettingsManager().schemas_file, 'w') as f:
            f.write(bad_schemas)

        # Test
        load_schemas_json()
        ts = get_testsuite_xml().get_testsuite(
            "load_schemas_json|infer_schemas")
        assert ts.worst_message == "ERROR"
        assert ts.num_tests == 1
        assert ts.get_testcase("invalid_schemas_json")
