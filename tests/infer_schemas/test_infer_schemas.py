from collections import Counter
from pathlib import Path

import frictionless

from src.FAIR_DS_DataQualityAssurance.settings import SettingsManager
from src.FAIR_DS_DataQualityAssurance.infer_schemas import (
    main, get_testsuite_xml)
from tests.setups import Setups


class Test_ReturnValues:
    def test_no_schema_file_provided_one_schema(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data, 3)
        Setups.GenFile.schema(get_schema)

        # Test
        main()

        pkg_file = SettingsManager().package_file

        assert Path(pkg_file).exists(), (
            "We expect to create a new package file as a result of this "
            "function")
        assert Path("schemas/schema_1.json").exists(), (
            "As no schemas were provided and we have a single data format, "
            "we expect a single new schema file named schema_1 by default")

        # We should find a package file with all three files assigned to our
        # new schema
        my_package = frictionless.Package(pkg_file)

        assert set([r.path for r in my_package.resources]) == set([
            "data/file_1.csv",
            "data/file_2.csv",
            "data/file_3.csv"])
        for r in my_package.resources:
            assert list(r.schema.values()) == ["schemas/schema_1.json"]

    def test_no_schema_file_provided_two_schemas(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data, N=2, schema=0)
        Setups.GenFile.schema(get_schema, schema=0)
        Setups.GenFile.data(get_data, schema=1)
        Setups.GenFile.schema(get_schema, schema=1)

        # Test
        main()

        pkg_file = SettingsManager().package_file

        assert Path(pkg_file).exists(), (
            "We expect to create a new schemas file as a result of this "
            "function")
        assert (Path("schemas/schema_1.json").exists() and
                Path("schemas/schema_2.json").exists()), (
            "As no schemas were provided and we have a two data formats, "
            "we expect a two new schema files")

        # We should find a package file with all three files assigned to our
        # new schema
        my_package = frictionless.Package(pkg_file)

        assert set([r.path for r in my_package.resources]) == set([
            "data/file_1.csv",
            "data/file_2.csv",
            "data/file_3.csv"])

        # we don't know which file was assigned to which schema until runtime,
        # but we do know that one schema should be associated with one file,
        # and the other with two files.
        c = Counter()
        for r in my_package.resources:
            c.update(r.schema.values())
        assert sorted(c.values()) == [1, 2]

    def test_misassigned_schema(self, get_data, get_schema):
        # Setup
        my_schemas = {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv",
                "data/file_3.csv"
            ]
        }

        Setups.GenFile.data(get_data, N=2, schema=0)
        Setups.GenFile.data(get_data, schema=1)
        Setups.GenFile.schema(get_schema, schema=0)
        Setups.GenFile.schemas_file(my_schemas)

        # Test
        main()

        pkg_file = SettingsManager().package_file

        assert Path(pkg_file).exists(), (
            "We expect to create a new package file as a result of this "
            "function")
        assert Path("schemas/schema_2.json").exists(), (
            "We should have a new schema which applies to only file_3.csv")

        # We should find a package file with all three files
        my_package = frictionless.Package(pkg_file)

        # we should have all three files in our resources
        assert set([r.path for r in my_package.resources]) == set([
            "data/file_1.csv",
            "data/file_2.csv",
            "data/file_3.csv"])

        c = Counter()

        for resource in my_package.resources:
            assert "$ref" in resource.schema
            c.update([resource.schema["$ref"]])


class Test_Messages:
    def test_no_schema_file_provided_one_schema(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data, 3)
        Setups.GenFile.schema(get_schema)

        assert Path("data/file_1.csv").exists()
        assert Path("data/file_2.csv").exists()
        assert Path("data/file_3.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        main()

        xml = get_testsuite_xml()

        # we should have a total of 4 testsuites in our xml
        assert len(xml.testsuites) == 4

        # We should have a single warning that we had no schemas JSON file
        ts = xml.get_testsuite("load_schemas_json|infer_schemas")
        assert ts.worst_message == "WARNING"
        assert ts.num_tests == 1
        assert ts.get_testcase("no_schemas_json")

        # There shouldn't be any issues that arise in the validation step,
        # since these are all generated by the script, just a success msg
        ts = xml.get_testsuite("validate_schemas_file|infer_schemas")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 1
        assert ts.get_testcase("schemas_file_passed_validation")

        # We should get three messages in guess_schema - one where we realize
        # we don't have a schema file, and two where we apply the schema file
        # generated to files 2 and 3
        ts = xml.get_testsuite("guess_schema|infer_schemas")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 3
        assert ts.get_testcase("found_matching_schema_data/file_1.csv")
        assert ts.get_testcase("found_matching_schema_data/file_2.csv")
        assert ts.get_testcase("found_matching_schema_data/file_3.csv")

        # No way of knowing which file was used to determine the schema, so
        # we can jsut count the kinds of titles we see in the testcases
        found_schema, no_schema, create_schema = 0, 0, 0

        for tc in ts.testcases:
            if "No Schema Files in" in tc.name:
                no_schema += 1
            if "Found Matching Schema for" in tc.name:
                found_schema += 1
            if "Creating Generic Schema" in tc.name:
                create_schema += 1

        assert no_schema == 0
        assert found_schema == 3
        assert create_schema == 0

        ts = xml.get_testsuite("schema_inference|infer_schemas")
        assert ts.get_testcase("saving_package_json")

    def test_no_schema_file_provided_two_schemas(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data, N=2, schema=0)
        Setups.GenFile.schema(get_schema, schema=0)
        Setups.GenFile.data(get_data, schema=1)
        Setups.GenFile.schema(get_schema, schema=1)

        # Test
        main()

        xml = get_testsuite_xml()

        # we should have a total of 4 testsuites in our xml
        assert len(xml.testsuites) == 4

        # We should have a single warning that we had no schemas JSON file
        ts = xml.get_testsuite("load_schemas_json|infer_schemas")
        assert ts.worst_message == "WARNING"
        assert ts.num_tests == 1
        assert ts.get_testcase("no_schemas_json")

        # There shouldn't be any issues that arise in the validation step,
        # since these are all generated by the script, just a success msg
        ts = xml.get_testsuite("validate_schemas_file|infer_schemas")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 1

        # We should get three messages in guess_schema - one where we realize
        # we don't have a schema file, and two where we apply the schema file
        # generated to files 2 and 3
        ts = xml.get_testsuite("guess_schema|infer_schemas")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 3
        assert ts.get_testcase("found_matching_schema_data/file_1.csv")
        assert ts.get_testcase("found_matching_schema_data/file_2.csv")
        assert ts.get_testcase("found_matching_schema_data/file_3.csv")

        # No way of knowing which file was used to determine the schema, so
        # we count
        found_schema, no_schema, create_schema = 0, 0, 0

        for tc in ts.testcases:
            if "No Schema Files in" in tc.name:
                no_schema += 1
            if "Found Matching Schema for" in tc.name:
                found_schema += 1
            if "Creating Generic Schema" in tc.name:
                create_schema += 1

        assert no_schema == 0
        assert found_schema == 3
        assert create_schema == 0

        ts = xml.get_testsuite("schema_inference|infer_schemas")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 1
        assert ts.get_testcase("saving_package_json")

    def test_misassigned_schema(self, get_data, get_schema):
        # Setup
        my_schemas = {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv",
                "data/file_3.csv"
            ]
        }

        Setups.GenFile.data(get_data, N=2, schema=0)
        Setups.GenFile.data(get_data, schema=1)
        Setups.GenFile.schema(get_schema, schema=0)
        Setups.GenFile.schemas_file(my_schemas)

        # Test
        main()

        xml = get_testsuite_xml()

        # we should have a total of 4 testsuites in our xml
        assert len(xml.testsuites) == 4

        # We should have a success message the we loaded the json
        ts = xml.get_testsuite("load_schemas_json|infer_schemas")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 1
        assert ts.get_testcase("found_schemas_json")

        # We should have a single file which failed validation, and a slew
        # of errors explaining why this schema didn't apply correctly to this
        # data file
        ts = xml.get_testsuite("validate_schemas_file|infer_schemas")
        assert ts.worst_message == "ERROR"
        assert ts.num_tests == 2
        assert ts.get_testcase(
            "failed_validation_data/file_3.csv:schemas/schema_1.json")
        assert ts.get_testcase(
            "invalid_schema_schemas/schema_1.json_on_data/file_3.csv")

        ts = xml.get_testsuite("guess_schema|infer_schemas")
        assert ts.worst_message == "WARNING"
        assert ts.num_tests == 2
        assert ts.get_testcase("no_schemas_matching_data/file_3.csv")
        assert ts.get_testcase("creating_generic_schema_schemas/schema_2.json")

        ts = xml.get_testsuite("schema_inference|infer_schemas")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 1
        assert ts.get_testcase("saving_package_json")
