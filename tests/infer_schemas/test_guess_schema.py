import json
from pathlib import Path

from src.FAIR_DS_DataQualityAssurance.infer_schemas import (
    guess_schema, get_testsuite_xml)


class GuessSchemaSetups:
    def setup_matching_schema_present(self, get_schema, get_data):
        get_data().to_csv("data/file_1.csv")

        with open("schemas/schema_1.json", 'w') as f:
            json.dump(get_schema(ignore_fields=["str", "bool"]), f)
        with open("schemas/schema_2.json", 'w') as f:
            json.dump(get_schema(), f)

    def setup_no_matching_schema_present(self, get_schema, get_data):
        get_data().to_csv("data/file_1.csv")

        with open("schemas/schema_1.json", 'w') as f:
            json.dump(get_schema(ignore_fields=["str", "bool"]), f)
        with open("schemas/schema_2.json", 'w') as f:
            json.dump(get_schema(ignore_fields=["bool"]), f)

    def setup_no_schema_present(self, get_data):
        get_data().to_csv("data/file_1.csv")


class Test_ReturnValues(GuessSchemaSetups):
    def test_matching_schema_present(self, get_schema, get_data):
        # Setup
        super().setup_matching_schema_present(get_schema, get_data)

        # Test
        ret_val = guess_schema("data/file_1.csv")
        assert ret_val == Path("schemas/schema_2.json")

    def test_no_matching_schema_present(self, get_schema, get_data):
        # Setup
        super().setup_no_matching_schema_present(get_schema, get_data)

        # Test
        ret_val = guess_schema("data/file_1.csv")
        assert ret_val == Path("schemas/schema_3.json")

    def test_no_schema_present(self, get_data):
        # Setup
        super().setup_no_schema_present(get_data)

        # Test
        ret_val = guess_schema("data/file_1.csv")
        assert ret_val == Path("schemas/schema_1.json")


class Test_Messages(GuessSchemaSetups):
    def test_matching_schema_present(self, get_schema, get_data):
        # Setup
        super().setup_matching_schema_present(get_schema, get_data)

        # Test
        guess_schema("data/file_1.csv")
        ts = get_testsuite_xml().get_testsuite("guess_schema|infer_schemas")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 1
        assert ts.get_testcase("found_matching_schema_data/file_1.csv")

    def test_no_matching_schema_present(self, get_schema, get_data):
        # Setup
        super().setup_no_matching_schema_present(get_schema, get_data)

        # Test
        guess_schema("data/file_1.csv")
        ts = get_testsuite_xml().get_testsuite("guess_schema|infer_schemas")
        assert ts.worst_message == "WARNING"
        assert ts.num_tests == 2
        assert ts.get_testcase("no_schemas_matching_data/file_1.csv")
        assert ts.get_testcase("creating_generic_schema_schemas/schema_3.json")

        assert Path("schemas/schema_3.json").exists()

    def test_no_schema_present(self, get_data):
        # Setup
        super().setup_no_schema_present(get_data)

        # Test
        guess_schema("data/file_1.csv")

        ts = get_testsuite_xml().get_testsuite("guess_schema|infer_schemas")
        assert ts.worst_message == "WARNING"
        assert ts.num_tests == 2
        assert ts.get_testcase("no_schemas_matching_data/file_1.csv")
        assert ts.get_testcase("creating_generic_schema_schemas/schema_1.json")

        assert Path("schemas/schema_1.json").exists()
