from src.FAIR_DS_DataQualityAssurance.infer_schemas import (
    get_data_files_in_schemas_json)


class Test_GetDataFilesInSchemasJSON:
    def test_get_file_list(self):
        # Setup
        my_schemas = {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv"
            ],
            "schemas/schema_2.json": [
                "data/file_3.csv"
            ]
        }

        # Test
        assert get_data_files_in_schemas_json(my_schemas) == [
            "data/file_1.csv",
            "data/file_2.csv",
            "data/file_3.csv"]
