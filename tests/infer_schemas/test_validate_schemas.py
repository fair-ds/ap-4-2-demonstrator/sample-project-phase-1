import json
from pathlib import Path

from src.FAIR_DS_DataQualityAssurance.infer_schemas import (
    validate_schemas_file, get_testsuite_xml)


class Setups:
    def setup_schema_file_valid(setup_from_schema):
        my_data = {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv"
            ],
            "schemas/schema_2.json": [
                "data/file_3.csv"
            ]
        }
        setup_from_schema(my_data)
        return my_data

    def setup_schema_file_missing(setup_from_schema):
        my_data = {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv"
            ],
            "schemas/schema_2.json": [
                "data/file_3.csv"
            ]
        }
        setup_from_schema(my_data, ignore_files=["schemas/schema_2.json"])
        return my_data

    def setup_data_file_mssing(setup_from_schema):
        my_data = {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv"
            ],
            "schemas/schema_2.json": [
                "data/file_3.csv"
            ]
        }
        setup_from_schema(my_data, ignore_files=["data/file_3.csv"])
        return my_data

    def setup_mismatched_schema(get_schema, setup_from_schema):
        my_data = {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv"
            ],
            "schemas/schema_2.json": [
                "data/file_3.csv"
            ]
        }
        setup_from_schema(my_data, ignore_files=["schemas/schema_2.json"])

        # get a schema, then add a field that we know doesn't exist in the
        # data
        schema = get_schema()
        schema["fields"].append({"name": "new_column", "type": "boolean"})
        with open("schemas/schema_2.json", 'w') as f:
            json.dump(schema, f)

        return my_data

    def setup_multiple_schemas_one_file(setup_from_schema):
        my_data = {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv"
            ],
            "schemas/schema_2.json": [
                "data/file_2.csv"
            ]
        }
        setup_from_schema(my_data)

        assert Path("schemas/schema_1.json").exists()
        assert Path("data/file_1.csv").exists()
        assert Path("data/file_2.csv").exists()
        assert Path("schemas/schema_2.json").exists()
        assert not Path("data/file_3.csv").exists()

        return my_data


class Test_ReturnValues:
    """
    Test Function Behaviour
    """
    def test_schema_file_valid(self, setup_from_schema):
        # Setup
        my_data = Setups.setup_schema_file_valid(setup_from_schema)

        # Test
        assert Path("schemas/schema_1.json").exists()
        assert Path("data/file_1.csv").exists()
        assert Path("data/file_2.csv").exists()
        assert Path("schemas/schema_2.json").exists()
        assert Path("data/file_3.csv").exists()

        ret_value = validate_schemas_file(my_data)
        assert ret_value == my_data, (
            "We expect all records related to the schema file that doesn't "
            "exist to have been removed, along with the schema file")

    def test_schema_file_missing(self, setup_from_schema):
        # Setup
        my_data = Setups.setup_schema_file_missing(setup_from_schema)

        # Test
        assert Path("schemas/schema_1.json").exists()
        assert Path("data/file_1.csv").exists()
        assert Path("data/file_2.csv").exists()
        assert not Path("schemas/schema_2.json").exists()
        assert Path("data/file_3.csv").exists()

        assert validate_schemas_file(my_data) == {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv"
            ]
        }, ("We expect all records related to the schema file that doesn't "
            "exist to have been removed, along with the schema file")

    def test_data_file_missing(self, setup_from_schema):
        # Setup
        my_data = Setups.setup_data_file_mssing(setup_from_schema)

        # Test
        assert Path("schemas/schema_1.json").exists()
        assert Path("data/file_1.csv").exists()
        assert Path("data/file_2.csv").exists()
        assert Path("schemas/schema_2.json").exists()
        assert not Path("data/file_3.csv").exists()

        ret_val = validate_schemas_file(my_data)
        assert ret_val == {
            "schemas/schema_1.json": [
                "data/file_1.csv",
                "data/file_2.csv"
            ],
            "schemas/schema_2.json": []
        }, ("We expect any missing data files to be removed from the schemas "
            "json. We leave the schema file.")

    def test_mismatched_schema(self, get_schema, setup_from_schema):
        # Setup
        my_data = Setups.setup_mismatched_schema(get_schema, setup_from_schema)

        # Test
        ret_val = validate_schemas_file(my_data)
        assert ret_val == my_data, (
            "We expect the schemas list to remain the same, we're just going "
            "to warn the user where the schemas didn't work")

    def test_multiple_schemas_one_file(self, setup_from_schema):
        # Setup
        my_data = Setups.setup_multiple_schemas_one_file(setup_from_schema)

        # Test
        ret_val = validate_schemas_file(my_data)
        assert ret_val == {
                "schemas/schema_1.json": [
                    "data/file_1.csv"],
                "schemas/schema_2.json": []
            }, (
            "We expect the duplicate file to be identified and removed from "
            "all of the schemas")


class Test_Messages:
    """
    Test Messages written to the XML
    """
    def test_schema_file_valid(self, setup_from_schema):
        # Setup
        my_data = Setups.setup_schema_file_valid(setup_from_schema)

        # Test
        validate_schemas_file(my_data)
        ts = get_testsuite_xml().get_testsuite(
            "validate_schemas_file|infer_schemas")

        validate_schemas_file(my_data)
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 1
        assert ts.get_testcase("schemas_file_passed_validation")

    def test_missing_schema_file(self, setup_from_schema):
        # Setup
        my_data = Setups.setup_schema_file_missing(setup_from_schema)

        # Test
        validate_schemas_file(my_data)
        ts = get_testsuite_xml().get_testsuite(
            "validate_schemas_file|infer_schemas")
        assert ts.worst_message == "WARNING"
        assert ts.num_tests == 1
        assert ts.get_testcase("missing_schema_schemas/schema_2.json")

    def test_missing_data_file(self, setup_from_schema):
        # Setup
        my_data = Setups.setup_data_file_mssing(setup_from_schema)

        # Test
        validate_schemas_file(my_data)
        ts = get_testsuite_xml().get_testsuite(
            "validate_schemas_file|infer_schemas")
        assert ts.worst_message == "ERROR"
        assert ts.num_tests == 1
        assert ts.get_testcase("missing_data_file_data/file_3.csv")

    def test_mismatches_schema(self, get_schema, setup_from_schema):
        # Setup
        my_data = Setups.setup_mismatched_schema(get_schema, setup_from_schema)

        # Test
        validate_schemas_file(my_data)
        ts = get_testsuite_xml().get_testsuite(
            "validate_schemas_file|infer_schemas")

        assert ts.worst_message == "ERROR"
        assert ts.num_tests == 2
        assert ts.get_testcase(
            "failed_validation_data/file_3.csv:schemas/schema_2.json")
        assert ts.get_testcase(
            "invalid_schema_schemas/schema_2.json_on_data/file_3.csv")

    def test_multiple_schemas_one_file(self, setup_from_schema):
        # Setup
        my_data = Setups.setup_multiple_schemas_one_file(setup_from_schema)

        # Test
        validate_schemas_file(my_data)

        ts = get_testsuite_xml().get_testsuite(
            "validate_schemas_file|infer_schemas")

        assert ts.worst_message == "ERROR"
        assert ts.num_tests >= 1  # We should get a couple errors here
        assert ts.get_testcase(
            "file_assigned_to_N_schemas_data/file_2.csv")
