from collections import Counter
from pathlib import Path

import frictionless

from src.FAIR_DS_DataQualityAssurance.utils.schema import get_schema_report
from tests.setups import Setups


class Test_Messages:
    def test_parameter_types(self, get_data, get_schema):
        """Test that all of the types we accept as parameters are valid"""
        # Setup
        Setups.GenFile.data(get_data)
        Setups.GenFile.schema(get_schema)

        assert Path("data/file_1.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        # filename as a string or a path
        get_schema_report("data/file_1.csv", "schemas/schema_1.json")
        get_schema_report(Path("data/file_1.csv"), "schemas/schema_1.json")

        # schema as a string or a path
        get_schema_report("data/file_1.csv", "schemas/schema_1.json")
        get_schema_report("data/file_1.csv", Path("schemas/schema_1.json"))

        # schema as a dict or a frictinless.Schema
        get_schema_report("data/file_1.csv", get_schema())
        get_schema_report("data/file_1.csv", frictionless.Schema(get_schema()))

    def test_good_file(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data)
        Setups.GenFile.schema(get_schema)

        assert Path("data/file_1.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        ret_val = get_schema_report("data/file_1.csv", "schemas/schema_1.json")
        assert ret_val == []

    def test_schema_missing_col(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data)
        Setups.GenFile.schema(get_schema, schema=1)

        assert Path("data/file_1.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        ret_val = get_schema_report("data/file_1.csv", "schemas/schema_1.json")

        assert len(ret_val) == 1, \
            "There should only be one tuple in the report"
        assert ret_val[0][0] == "column-mismatch", \
            "The only code should be that the columns don't match"

        for item in ret_val:
            fail_type, message = item
            assert isinstance(fail_type, str)
            assert isinstance(message, str)

    def test_data_missing_col(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data, schema=1)
        Setups.GenFile.schema(get_schema)

        assert Path("data/file_1.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        ret_val = get_schema_report("data/file_1.csv", "schemas/schema_1.json")

        assert len(ret_val) == 1, \
            "There should only be one tuple in the report"
        assert ret_val[0][0] == "column-mismatch", \
            "The only code should be that the columns don't match"

        for item in ret_val:
            fail_type, message = item
            assert isinstance(fail_type, str)
            assert isinstance(message, str)

    def test_out_of_order_cols(self, get_data, get_schema):
        # Setup
        col_order = [
            'char_1', 'bool_1', 'str_1', 'date_1',
            'float_1', 'int_1', 'geopoint_1']
        df = get_data()[col_order]
        df.to_csv("data/file_1.csv")
        Setups.GenFile.schema(get_schema)

        # Test
        ret_val = get_schema_report("data/file_1.csv", "schemas/schema_1.json")

        c = Counter([code for code, msg in ret_val])

        assert c["incorrect-label"] == 2, \
            ("We should have an incorrect label message for each of the "
             "swapped columns")
        assert c["type-error"] >= 10, \
            ("We should have actually have 20 errors, two for each row in the "
             "dataset, but we cap this at 10 for readabiity.")
        for item in ret_val:
            fail_type, message = item
            assert isinstance(fail_type, str)
            assert isinstance(message, str)

    def test_misspelled_col(self, get_data, get_schema):
        # Setup
        df = get_data().rename(columns={"int_1": "integer_1"})
        df.to_csv("data/file_1.csv")
        Setups.GenFile.schema(get_schema)

        # Test
        ret_val = get_schema_report("data/file_1.csv", "schemas/schema_1.json")

        c = Counter([code for code, msg in ret_val])

        assert c["column-mismatch"] == 1, \
            ("We should have one column mismatch message: 'int' != 'integer'")

        for item in ret_val:
            fail_type, message = item
            assert isinstance(fail_type, str)
            assert isinstance(message, str)
