import os
from pathlib import Path

import frictionless
import numpy as np
import pandas as pd
import pytest

from src.FAIR_DS_DataQualityAssurance.utils.load_data import (
    load_csv_with_schema)
from tests.setups import Setups
from tests.util import DEFAULT_COLS


def gen_test_data(
        permutations=10,
        min_cols=3, max_cols=20,
        seed=42):
    test_data = []
    np.random.seed(seed)
    for _ in range(permutations):
        cols = [
            np.random.choice(DEFAULT_COLS)
            for _
            in range(np.random.randint(min_cols, max_cols))]
        formats = {}
        if "geopoint" in cols:
            formats["geopoint"] = np.random.choice(
                ["default", "object", "array"])
        test_data.append((cols, formats))
    return test_data


@pytest.mark.parametrize("fields, formats", gen_test_data())
def test_simple_load(get_data, get_schema, fields, formats):
    # Setup
    Setups.GenFile.data(get_data, fields=fields, formats=formats)
    Setups.GenFile.schema(get_schema, fields=fields, formats=formats)

    # Test
    assert Path("data/file_1.csv").exists()
    assert Path("schemas/schema_1.json").exists()

    my_schema = frictionless.Schema("schemas/schema_1.json")
    ret_val = load_csv_with_schema(
        "data/file_1.csv", my_schema, repair_schema=False)

    assert isinstance(ret_val, pd.DataFrame)


def test_load_with_repair_schema(get_data, get_schema):
    """We still want to load a file if we get a schema that doesn't quite
    match the data. We can have this function check if it can just load the
    column in as an object if one of the fields is incorrectly typed or
    malformed"""
    # Setup
    Setups.GenFile.data(get_data, fields=["char", "str", "bool"])
    Setups.GenFile.schema(get_schema, fields=["char", "int", "bool"])

    # Test
    assert Path("data/file_1.csv").exists()
    assert Path("schemas/schema_1.json").exists()

    my_schema = frictionless.Schema("schemas/schema_1.json")
    ret_val = load_csv_with_schema("data/file_1.csv", my_schema)

    assert isinstance(ret_val, pd.DataFrame)


geo_test_data = [t for t in gen_test_data() if "geopoint" in t[0]]


@pytest.mark.parametrize("fields, formats", geo_test_data)
def test_geodata_load(get_data, get_schema, fields, formats):
    Setups.GenFile.data(get_data, fields=fields, formats=formats)
    Setups.GenFile.schema(get_schema, fields=fields, formats=formats)

    # Test
    assert Path("data/file_1.csv").exists()
    assert Path("schemas/schema_1.json").exists()

    my_schema = frictionless.Schema("schemas/schema_1.json")
    load_csv_with_schema("data/file_1.csv", my_schema)


@pytest.mark.skipif(
    "jh-admin" not in os.getcwd(), reason="Only run on local machine")
def test_sample_data_load():
    Setups.LoadData.setup("biodiv")

    assert Path("data/biodiv_cat.csv").exists()
    assert Path("schemas/biodiv_schema.json").exists()

    my_schema = frictionless.Schema("schemas/biodiv_schema.json")
    load_csv_with_schema("data/biodiv_cat.csv", my_schema)


@pytest.mark.skipif(
    "jh-admin" not in os.getcwd(), reason="Only run on local machine")
def test_geodata_multiple_columns():
    """Instead of requiring users to have their geo data in frictionless'
    format, we can specify a setting indicating the columns to be used"""

    Setups.LoadData.setup("biodiv")

    # Test
    my_schema = frictionless.Schema("schemas/biodiv_schema.json")
    load_csv_with_schema(
        "data/biodiv_cat.csv",
        schema_data=my_schema)
