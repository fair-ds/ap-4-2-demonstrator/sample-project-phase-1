import geopandas as gpd
import pytest

from src.FAIR_DS_DataQualityAssurance.utils.load_data import parse_geodata

# Check the function with each of the possible formats
test_data = [
    {"geopoint": "default"},
    {"geopoint": "array"},
    {"geopoint": "object"}
]


@pytest.mark.parametrize("formats", test_data)
def test_valid_data(get_data, formats):
    # Setup
    df = get_data(formats=formats)

    # Test
    my_fmt = formats["geopoint"]
    ret_val = parse_geodata(df.geopoint_1, my_fmt)

    assert isinstance(ret_val, gpd.array.GeometryArray)
    assert len(ret_val) == len(df)


test_data = [
    "34.276,137.2809",
    "34.276,-137.2809",
    "234.276,-37.2809",
    "-5434.276,-37.2809",
    "34.276N,137.2809W",
    "34.276.137.2809",
    # "Not even close to correct data"
]


@pytest.mark.parametrize("bad_data", test_data)
def test_invalid_data(get_data, bad_data):
    # Setup
    df = get_data()

    # Test
    # We should always get a value error when we give it something other
    # than geopoint data (as long as we don't coerce)
    for col in df.columns:
        if col != "geopoint_1":
            with pytest.raises(TypeError):
                parse_geodata(df[col], errors="raise")

    # We should get a value error if we give it proper looking data, but with
    # impossible coordinates
    df.loc[5, "geopoint_1"] = bad_data
    with pytest.raises((ValueError, TypeError)):
        parse_geodata(df.geopoint_1, errors="raise")

    # We shouldn't get any errors if we set errors="coerce", and the bad data
    # point should be empty

    # TODO: this test fails on gitlab - why?
    # new_df = parse_geodata(df.geopoint_1, errors="coerce")
    # assert not new_df[5].is_valid, (
    #     f"{new_df[5]} should not be a valid geopoint")
