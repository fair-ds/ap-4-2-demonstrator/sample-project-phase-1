from pathlib import Path
import pytest

from src.FAIR_DS_DataQualityAssurance.utils.file_io import gather_files
from tests.setups import Setups


def test_valid_path(get_data):
    Setups.GenFile.data(get_data, N=4)

    ret_val = gather_files("data")
    assert isinstance(ret_val, list)
    assert len(ret_val) == 4
    assert set(ret_val) == set([
        Path("data/file_1.csv"),
        Path("data/file_2.csv"),
        Path("data/file_3.csv"),
        Path("data/file_4.csv")])


def test_empty_directory():
    ret_val = gather_files("data")
    assert isinstance(ret_val, list)
    assert len(ret_val) == 0
    assert ret_val == []


def test_invalid_path():
    with pytest.raises(FileNotFoundError):
        gather_files("doesntexist")
