from pathlib import Path

from src.FAIR_DS_DataQualityAssurance.infer_schemas import get_all_data_files


class Test_GetAllDataFiles:
    """
    I can find all files in the data folder
    I can find all files, even if one is in a subfolder
    I can ignore files that don't match an expected file pattern
    """

    def test_get_all_data_files_present(self, get_data):
        # Setup
        Path("data/subfolder").mkdir()

        files = [
            "data/file_1.csv",
            "data/file_2.csv",
            "data/subfolder/file_3.csv"]

        for f in files:
            get_data().to_csv(f)

        # Test
        assert get_all_data_files() == files, (
            "We should see all three csvs, even the one in a subfolder")

    def test_get_all_data_files_csvs_only(self, get_data):
        # Setup
        files = [
            "data/file_1.csv",
            "data/file_2.csv"]

        for f in files:
            get_data().to_csv(f)

        get_data().to_pickle("data/file_3.pkl")

        # Test
        assert get_all_data_files() == files, (
            "We should see only two csvs and ignore the non csv file")
