import json
import logging
from pathlib import Path
import shutil
import yaml

from src.FAIR_DS_DataQualityAssurance.settings import SettingsManager
from src.FAIR_DS_DataQualityAssurance.utils.package import build_package_file
import tests
from tests.util import SCHEMA_TYPE

logger = logging.getLogger(__name__)


def file_num():
    return len(list(Path("data").glob("file_*.csv"))) + 1


def schema_num():
    return len(list(Path("schemas").glob("schema_*.json"))) + 1


def _move_file(filename, dest_folder):
    src = Path(tests.__path__[0]) / "sample_data" / filename
    dest = Path(dest_folder) / filename
    logger.info("moving %s from %s to %s", filename, src, dest)
    shutil.copy(src, dest)


class Setups:
    class GenFile:
        def file_num():
            return len(list(Path("data").glob("file_*.csv"))) + 1

        def data(
                get_data,
                N: int = 1, schema: int = 0,
                fields: list[str] = None,
                formats: dict = {}):

            for _ in range(N):
                filename = f"data/file_{file_num()}.csv"
                if fields:
                    df = get_data(fields=fields, formats=formats)
                else:
                    df = get_data(
                        ignore_fields=SCHEMA_TYPE[schema], formats=formats)
                df.to_csv(filename)

        def schema(
                get_schema, schema: int = 0,
                fields: list[str] = None, formats: dict = {}):
            filename = f"schemas/schema_{schema_num()}.json"
            if fields:
                data = get_schema(fields=fields, formats=formats)
            else:
                data = get_schema(
                    ignore_fields=SCHEMA_TYPE[schema], formats=formats)
            with open(filename, 'w') as f:
                json.dump(data, f, indent=4)

        def schemas_file(schemas: dict):
            with open(SettingsManager().schemas_file, 'w') as f:
                json.dump(schemas, f, indent=4)

        def settings(settings_dict: dict):
            with open('settings.yml', 'w') as f:
                yaml.dump(settings_dict, f)

        def package(schemas: dict):
            if not SettingsManager().schemas_file.exists():
                Setups.GenFile.schemas_file(schemas)
            with open(SettingsManager().package_file, 'w') as f:
                json.dump(build_package_file(schemas), f, indent=4)

    class LoadData:
        datasets = {
            "biodiv": {
                "data": [
                    "biodiv_cat.csv",
                    "biodiv_lynx.csv"],
                "schemas": [
                    "biodiv_schema.json"
                ]
            },
            "carrental": {
                "data": [
                    "CarRentalData.csv"
                ],
                "schemas": [
                    "CarRentalData_schema.json"
                ]
            }
        }

        def setup(dataset, data=True, schema=True):
            dataset_files = Setups.LoadData.datasets[dataset]

            if data:
                for filename in dataset_files["data"]:
                    _move_file(filename, "data")
            if schema:
                for filename in dataset_files["schemas"]:
                    _move_file(filename, "schemas")

    class Full:
        def valid(setup_from_schema):
            my_data = {
                "schemas/schema_1.json": [
                    "data/file_1.csv",
                    "data/file_2.csv"
                ],
                "schemas/schema_2.json": [
                    "data/file_3.csv"
                ]
            }
            setup_from_schema(my_data)
            with open("schemas/schemas.json", 'w') as f:
                json.dump(my_data, f)

            assert Path("schemas/schemas.json").exists()
            assert Path("schemas/schema_1.json").exists()
            assert Path("data/file_1.csv").exists()
            assert Path("data/file_2.csv").exists()
            assert Path("schemas/schema_2.json").exists()
            assert Path("data/file_3.csv").exists()

            return my_data

        def custom(setup_from_schema, my_data):
            setup_from_schema(my_data)
            with open("schemas/schemas.json", 'w') as f:
                json.dump(my_data, f)

            return my_data
