import os
from pathlib import Path

import pytest

from src.FAIR_DS_DataQualityAssurance.validate import (
    validate_file, get_testsuite_xml)
from tests.setups import Setups


class Test_Messages:
    def test_clean_schema_file(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data)
        Setups.GenFile.schema(get_schema)

        assert Path("data/file_1.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        validate_file("data/file_1.csv", "schemas/schema_1.json", 1)

        # The only output from the validation script should be the xml
        ts = get_testsuite_xml().get_testsuite("validate_file|data/file_1.csv")
        assert ts.worst_message == "SUCCESS"
        assert ts.num_tests == 1
        assert ts.get_testcase(
            "data/file_1.csv|schema_validated_schemas/schema_1.json")

    def test_bad_schema_file(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data)
        Setups.GenFile.schema(get_schema, schema=1)

        assert Path("data/file_1.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        validate_file("data/file_1.csv", "schemas/schema_1.json", 1)
        xml = get_testsuite_xml()
        ts = xml.get_testsuite("validate_file|data/file_1.csv")

        # We should get a bunch of

        assert ts.get_testcase(
            "data/file_1.csv|schema violation:column-mismatch")

    bad_geodata = [
        ("37.2809,134.276", ),
        ("37.2809,-134.276", ),
        ("337.2809,34.276", ),
        ("-337.2809,34.276", ),
        ("37.2809 N,34.276 E", ),
        ("Up,34.276", )
    ]

    @pytest.mark.parametrize("bad_data", bad_geodata)
    def test_bad_geodata(self, get_data, get_schema, bad_data):
        """Inspired by a mistake I just made, what if the user gives us valid
        looking geopoint data, but one of the latitudes is >/< 90? What about
        just weird looking or bad data in general?"""

        # Setup
        df = get_data()
        df.loc[5, "geopoint_1"] = bad_data
        df.to_csv("data/file_1.csv")
        Setups.GenFile.schema(get_schema)

        assert Path("data/file_1.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        validate_file("data/file_1.csv", "schemas/schema_1.json", 1)
        xml = get_testsuite_xml()
        ts = xml.get_testsuite("validate_file|data/file_1.csv")

        assert ts.get_testcase(
            "data/file_1.csv|schema violation:type-error")

    @pytest.mark.skipif(
        "jh-admin" not in os.getcwd(), reason="Only run on local machine")
    def test_sample_data(self):
        # Setup
        Setups.LoadData.setup("biodiv")

        # Test
        validate_file("data/biodiv_cat.csv", "schemas/biodiv_schema.json")

        xml = get_testsuite_xml()
        ts = xml.get_testsuite("validate_file|data/biodiv_cat.csv")

        # TODO: POINT(nan, nan) values should not trigger type-errors
        # ts.get_testcase((
        #     "data/biodiv_cat.csv|"
        #     "schema_validated_schemas/biodiv_schema.json"))
