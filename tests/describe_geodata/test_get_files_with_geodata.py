import json
from pathlib import Path

from src.FAIR_DS_DataQualityAssurance.describe_geo import (
    get_files_with_geodata)

from src.FAIR_DS_DataQualityAssurance.settings import SettingsManager
from src.FAIR_DS_DataQualityAssurance.utils.package import build_package_file

from tests.setups import Setups


def test_find_files(get_data, get_schema):
    """We create five data files, 1, 2, 5 & 6 are non geo, 3 & 4 contain a
    geopoint column. 1/2 and 5/6 share schemas"""

    # Setup
    Setups.GenFile.data(get_data, N=2, schema=7)
    Setups.GenFile.schema(get_schema, schema=7)

    Setups.GenFile.data(get_data, N=2, schema=11)  # date, int, geopoint
    Setups.GenFile.schema(get_schema, schema=11)

    Setups.GenFile.data(get_data, N=2, schema=13)  # char, str, bool, int
    Setups.GenFile.schema(get_schema, schema=13)

    my_schemas = {
        "schemas/schema_1.json": [
            "data/file_1.csv",
            "data/file_2.csv"
        ],
        "schemas/schema_2.json": [
            "data/file_3.csv",
            "data/file_4.csv"
        ],
        "schemas/schema_3.json": [
            "data/file_5.csv",
            "data/file_6.csv"
        ]
    }
    package_file = SettingsManager().package_file

    with open(package_file, 'w') as f:
        json.dump(build_package_file(my_schemas), f, indent=4)

    for schema, data_files in my_schemas.items():
        assert Path(schema).exists()
        for data_file in data_files:
            assert Path(data_file).exists()
    assert package_file.exists()

    # Test
    ret_val = get_files_with_geodata()

    # The return value should be a list of tuples. Each tuple is a data file
    # and it's associated schema file
    assert isinstance(ret_val, list)
    for i in ret_val:
        assert isinstance(i, tuple)
        assert len(i) == 2
        assert isinstance(i[0], Path)
        assert isinstance(i[1], list)
        for j in i[1]:
            assert isinstance(j, str)
        assert i[0].exists()

    d1, cols1 = ret_val[0]
    d2, cols2 = ret_val[1]
    assert Path(d1) == Path("data/file_3.csv")
    assert cols1 == ["geopoint_1"]
    assert Path(d2) == Path("data/file_4.csv")
    assert cols2 == ["geopoint_1"]


def test_find_files_none_exist(get_data, get_schema):
    """We create 4 data files, none of which have geo data."""

    # Setup
    Setups.GenFile.data(get_data, N=2, schema=7)
    Setups.GenFile.schema(get_schema, schema=7)

    Setups.GenFile.data(get_data, N=2, schema=13)  # char, str, bool, int
    Setups.GenFile.schema(get_schema, schema=13)

    my_schemas = {
        "schemas/schema_1.json": [
            "data/file_1.csv",
            "data/file_2.csv"
        ],
        "schemas/schema_2.json": [
            "data/file_3.csv",
            "data/file_4.csv"
        ]
    }

    package_file = SettingsManager().package_file

    with open(package_file, 'w') as f:
        json.dump(build_package_file(my_schemas), f, indent=4)

    for schema, data_files in my_schemas.items():
        assert Path(schema).exists()
        for data_file in data_files:
            assert Path(data_file).exists()
    assert package_file.exists()

    # Test
    ret_val = get_files_with_geodata()

    # The return value should be an empty list
    assert isinstance(ret_val, list)
    assert len(ret_val) == 0


def test_user_provides_geodata(get_data, get_schema):
    Setups.GenFile.data(get_data, N=2, schema=11)
    Setups.GenFile.schema(get_schema, schema=11)

    Setups.GenFile.data(get_data, fields=["bool", "float", "float"])
    Setups.GenFile.schema(get_schema, fields=["bool", "float", "float"])

    my_schemas = {
        "schemas/schema_1.json": [
            "data/file_1.csv",
            "data/file_2.csv"
        ],
        "schemas/schema_2.json": [
            "data/file_3.csv"
        ]
    }

    my_geodata = {
        "data/file_3.csv": {
            "latitude": "float_1",
            "longitude": "float_2"
        }
    }

    package_file = SettingsManager().package_file

    with open(package_file, 'w') as f:
        json.dump(build_package_file(my_schemas), f, indent=4)

    # Test
    ret_val = get_files_with_geodata(geodata=my_geodata)

    assert len(ret_val) == 3
    assert Path(ret_val[0][0]) == Path("data/file_1.csv")
    assert ret_val[0][1] == ["geopoint_1"]
    assert Path(ret_val[1][0]) == Path("data/file_2.csv")
    assert ret_val[1][1] == ["geopoint_1"]
    assert Path(ret_val[2][0]) == Path("data/file_3.csv")
    assert ret_val[2][1] == ["geo"]
