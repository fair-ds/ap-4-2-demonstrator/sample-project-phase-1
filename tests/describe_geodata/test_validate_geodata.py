from JUnitXMLGen import TestSuites
from src.FAIR_DS_DataQualityAssurance.describe_geo import validate_geodata
from src.FAIR_DS_DataQualityAssurance.settings import SettingsManager

from tests.setups import Setups


def test_file_with_schema(get_data, get_schema):
    # Setup
    Setups.GenFile.data(get_data, schema=11)  # date, int, geopoint
    Setups.GenFile.schema(get_schema, schema=11)

    my_schemas = {
        "schemas/schema_1.json": [
            "data/file_1.csv"
        ]
    }

    Setups.GenFile.schemas_file(my_schemas)

    # Test
    validate_geodata("data/file_1.csv", "geopoint_1")

    xml = TestSuites(filename=SettingsManager().validation_report)

    assert xml.total_tests == 1
    assert xml.worst_message == "SUCCESS"

    ts = xml.get_testsuite("Validate Geodata|Describe Geo")
    assert ts.num_tests == 1
    assert ts.worst_message == "SUCCESS"

    tc = ts.get_testcase("data/file_1.csv|geo_qc_passed_geopoint_1")
    assert tc.name == (
        "\"data/file_1.csv\": "
        "No Geospatial Quality Issues Noted in \"geopoint_1\"")
