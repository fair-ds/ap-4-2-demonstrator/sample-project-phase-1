from src.FAIR_DS_DataQualityAssurance.settings import SettingsManager

def test_defaults():
    settings = SettingsManager()

    assert settings.project_name == "My Project"
    assert settings.file_delimiter == ','
    assert isinstance(settings.input, dict)
    assert settings.input["data_folder"] == "data"
    assert settings.input["schema_folder"] == "schemas"
    assert settings.input["schemas_file"] == "schemas.json"
