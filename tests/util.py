import datetime
from itertools import chain, combinations
import json

import numpy as np
import pandas as pd


DEFAULT_COLS = ["char", "str", "bool", "date", "float", "int", "geopoint"]


# get all combinations of a list.
# From https://stackoverflow.com/a/5898031
def all_subsets(ss):
    return chain(*map(lambda x: combinations(ss, x), range(0, len(ss))))


# our "schemas" are based of off iterating over combinations of removing
# columns from our test dataset. We gen this list once and reference it
# whenever we need to make a new data file or schema
SCHEMA_TYPE = {
    i: list(ignore_cols)
    for i, ignore_cols in
    enumerate(all_subsets(DEFAULT_COLS))}


def gen_dummy_data(
        records=10, seed=42,
        index=True,
        fields: list[str] = DEFAULT_COLS,
        ignore_fields: list[str] = [],
        missingness: dict = {},
        formats: dict = {}) -> pd.DataFrame:
    """Generate Dummy data for testing

    Args:
        records (int):
            The number of records to generate
        seed (int):
            The random seed to use when generating
        ignore_fields (list[str]):
            Fields to skip when generating the dataset
        missingness (dict):
            A dictionary of column:float pairs indicating what percent of
            values in a given column should be null.

    Returns:
        pandas.DataFrame
    """
    def get_geo(fmt="default", precision=4):
        def _get_geo_val(limit):
            return (np.random.randint(-limit, limit) +
                    round(np.random.rand(), precision))

        if fmt == "default":
            return f"{_get_geo_val(179)},{_get_geo_val(89)}"
        elif fmt == "array":
            return [_get_geo_val(179), _get_geo_val(89)]
        elif fmt == "object":
            return {
                "lon": _get_geo_val(179),
                "lat": _get_geo_val(89)
            }

    np.random.seed(seed)
    data = {}

    my_fields = fields.copy()
    for to_ignore in ignore_fields:
        my_fields.remove(to_ignore)

    for field in my_fields:
        col_num = len([c for c in data.keys() if field in c]) + 1
        if field == "char":
            data[f"char_{col_num}"] = [
                np.random.choice(['X', 'Y', 'Z']) for i in range(records)]
        elif field == "str":
            data[f"str_{col_num}"] = [
                np.random.choice(['alpha', 'beta', 'gamma'])
                for i in range(records)]
        elif field == "bool":
            data[f"bool_{col_num}"] = [
                np.random.choice([True, False]) for i in range(records)]
        elif field == "date":
            data[f"date_{col_num}"] = [
                np.random.choice(
                    pd.date_range(
                        datetime.datetime(2000, 1, 1),
                        datetime.datetime(2020, 12, 31)))
                for i in range(records)]
        elif field == "float":
            data[f"float_{col_num}"] = np.random.randn(records)
        elif field == "int":
            data[f"int_{col_num}"] = np.random.randint(0, 4, records)
        elif field == "geopoint":
            fmt = formats.get("geopoint", "default")
            data[f"geopoint_{col_num}"] = [
                get_geo(fmt) for i in range(records)]

    df = pd.DataFrame(data)

    if index:
        df['index'] = [i for i in range(records)]
        df.set_index("index", inplace=True)

    return df


def gen_schema(
        fields: list[str] = DEFAULT_COLS,
        ignore_fields: list[str] = [],
        index="index",
        formats: dict = {}) -> dict:
    """Generate a schema to go along with the dummy data

    Args:
        ignore_fields (list[str]):
            a list of fields to leave out of the schema.

    Returns:
        dict
    """
    my_fields = fields.copy()
    for to_ignore in ignore_fields:
        my_fields.remove(to_ignore)

    schema_data = {
        "fields": [
        ],
        "missingValues": [
            "NaN",
            "-",
            "",
            " "
        ]
    }

    if index:
        schema_data["primaryKey"] = index
        schema_data["fields"].append({
                "name": "index",
                "type": "integer"
            })

    for field in my_fields:
        col_num = len(
            [c for c in schema_data["fields"] if field in c["name"]]) + 1
        if field == "char":
            field_data = {
                "name": f"char_{col_num}",
                "type": "string",
                "constraints": {
                    "required": True,
                    "enum": ["X", "Y", "Z"],
                    "minLength": 1,
                    "maxLength": 1
                }
            }
        elif field == "str":
            field_data = {
                "name": f"str_{col_num}",
                "type": "string",
            }
        elif field == "bool":
            field_data = {
                "name": f"bool_{col_num}",
                "type": "boolean"
            }
        elif field == "date":
            field_data = {
                "name": f"date_{col_num}",
                "type": "date"
            }
        elif field == "float":
            field_data = {
                "name": f"float_{col_num}",
                "type": "number",
            }
        elif field == "int":
            field_data = {
                "name": f"int_{col_num}",
                "type": "integer",
                "constraints": {
                    "minimum": 0
                }
            }
        elif field == "geopoint":
            field_data = {
                "name": f"geopoint_{col_num}",
                "type": "geopoint",
                "format": formats.get("geopoint", "default")
            }

        schema_data["fields"].append(field_data)

    return schema_data


def create_setup_from_schemas_file(
        schemas: dict,
        ignore_files: list[str] = []) -> None:

    num_schemas = len(schemas.keys())
    assert num_schemas < len(SCHEMA_TYPE.keys())

    for schema_info, drop_cols in zip(schemas.items(), SCHEMA_TYPE.values()):
        schema_file, data_files = schema_info
        if schema_file not in ignore_files:
            with open(schema_file, 'w') as f:
                json.dump(gen_schema(ignore_fields=drop_cols), f)
        for data_file in data_files:
            if data_file not in ignore_files:
                gen_dummy_data(ignore_fields=drop_cols).to_csv(data_file)
