from pathlib import Path
import json
import os

import frictionless
import pytest

from src.FAIR_DS_DataQualityAssurance.utils.package import build_package_file
from tests.setups import Setups

testdata = [True, False]


@pytest.mark.parametrize("userefs", testdata)
def test_clean_files(setup_from_schema, userefs):
    # Setup
    Setups.Full.valid(setup_from_schema)

    # Test
    ret_value = build_package_file("schemas/schemas.json", use_refs=userefs)

    assert isinstance(ret_value, frictionless.Package)
    assert ret_value.profile == 'tabular-data-package'
    assert len(ret_value.resources) == 3
    assert set(ret_value.resource_names) == set(
        ["file_1", "file_2", "file_3"])


def test_parameter_types(setup_from_schema):
    # Setup
    Setups.Full.valid(setup_from_schema)

    # Test
    assert build_package_file("schemas/schemas.json")
    assert build_package_file(Path("schemas/schemas.json"))

    with open("schemas/schemas.json") as f:
        schemas_data = json.load(f)
    assert build_package_file(schemas_data)


def test_no_schemas_present(get_data):
    # Setup
    Setups.GenFile.data(get_data, N=2)

    assert os.listdir("schemas") == []

    # Test
    with pytest.raises(FileNotFoundError):
        build_package_file("schemas/schemas.json")
