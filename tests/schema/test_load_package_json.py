import json
import pytest

import frictionless

from src.FAIR_DS_DataQualityAssurance.utils.package import (
    build_package_file, load_package_json)
from src.FAIR_DS_DataQualityAssurance.settings import SettingsManager
from tests.setups import Setups

testdata = [True, False]


@pytest.mark.parametrize("userefs", testdata)
def test_clean_file(setup_from_schema, userefs):
    # Setup
    Setups.Full.valid(setup_from_schema)
    sm = SettingsManager()

    with open(sm.package_file, 'w') as f:
        json.dump(build_package_file("schemas/schemas.json", userefs), f)
    assert sm.package_file.exists()

    # Test
    pkg = load_package_json(sm.package_file)

    assert pkg.profile == 'tabular-data-package'
    assert len(pkg.resources) == 3
    assert set(pkg.resource_names) == set(["file_1", "file_2", "file_3"])


def test_malformed_package_file(setup_from_schema):
    """What if the package file is present, but not in a proper format"""
    # Setup
    sm = SettingsManager()
    Setups.Full.valid(setup_from_schema)
    with open(sm.package_file, 'w') as f:
        f.write("BAD DATA")

    with pytest.raises(frictionless.exception.FrictionlessException):
        load_package_json(sm.package_file)
