from pathlib import Path

import frictionless
import pytest

from src.FAIR_DS_DataQualityAssurance.utils.schema import load_schema
from tests.setups import Setups


class Test_GetSchema:
    def test_valid_file(self, get_schema):
        # Setup
        Setups.GenFile.schema(get_schema)

        # Test
        # This function should work with the schema parameter being passed in
        # as either a string, Path, dict, or Schmea variable
        my_schema = load_schema("schemas/schema_1.json")

        assert isinstance(my_schema, frictionless.Schema)
        assert dict(my_schema) == get_schema()

    def test_missing_file(self):
        # Setup
        assert not Path("schemas/schema_1.json").exists()

        # Test
        with pytest.raises(FileNotFoundError):
            load_schema("schemas/schema_1.json")
