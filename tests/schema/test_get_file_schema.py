import json
from pathlib import Path

import frictionless
import pytest

from src.FAIR_DS_DataQualityAssurance.utils.package import (
    get_file_schema, build_package_file)
from tests.setups import Setups


testdata = [
    ("data/file_1.csv", "schemas/schema_1.json"),
    ("data/file_2.csv", "schemas/schema_1.json"),
    ("data/file_3.csv", "schemas/schema_2.json")
]


@pytest.mark.parametrize("filename, expected", testdata)
def test_simple(setup_from_schema, filename, expected):
    # Setup
    Setups.Full.valid(setup_from_schema)
    build_package_file("schemas/schemas.json")

    assert Path(filename).exists()

    # Test
    ret_val = get_file_schema(filename)
    assert isinstance(ret_val, frictionless.Schema)

    with open(expected) as f:
        expected_schema = frictionless.Schema(json.load(f))

    assert ret_val == expected_schema


def test_file_not_found(setup_from_schema):
    # Setup
    Setups.Full.valid(setup_from_schema)
    build_package_file("schemas/schemas.json")

    # Test
    with pytest.raises(FileNotFoundError):
        get_file_schema("data/notarealfile.csv")
