import json
from pathlib import Path

import frictionless

from src.FAIR_DS_DataQualityAssurance.utils.schema import validate_schema
from tests.setups import Setups


class Test_ReturnValues:
    def test_valid_file(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data)
        Setups.GenFile.schema(get_schema)

        assert Path("data/file_1.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        # This function should work with the schema parameter being passed in
        # as either a string, Path, dict, or Schmea variable

        assert validate_schema("data/file_1.csv", "schemas/schema_1.json")
        assert validate_schema(
            "data/file_1.csv", Path("schemas/schema_1.json"))

        with open("schemas/schema_1.json") as f:
            my_schema_dict = json.load(f)
        assert validate_schema("data/file_1.csv", my_schema_dict)
        assert validate_schema(
            "data/file_1.csv", frictionless.Schema(my_schema_dict))

    def test_invalid_file(self, get_data, get_schema):
        # Setup
        Setups.GenFile.data(get_data, schema=1)
        Setups.GenFile.schema(get_schema)

        assert Path("data/file_1.csv").exists()
        assert Path("schemas/schema_1.json").exists()

        # Test
        # This function should work with the schema parameter being passed in
        # as either a string, Path, dict, or Schmea variable
        assert not validate_schema("data/file_1.csv", "schemas/schema_1.json")
        assert not validate_schema(
            "data/file_1.csv", Path("schemas/schema_1.json"))

        with open("schemas/schema_1.json") as f:
            my_schema_dict = json.load(f)
        assert not validate_schema("data/file_1.csv", my_schema_dict)
        assert not validate_schema(
            "data/file_1.csv", frictionless.Schema(my_schema_dict))
