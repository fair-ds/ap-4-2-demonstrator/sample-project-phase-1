import json
from pathlib import Path

from src.FAIR_DS_DataQualityAssurance.utils.package import get_package_file
from src.FAIR_DS_DataQualityAssurance.settings import SettingsManager
from tests.setups import Setups


def test_parameters(setup_from_schema):
    # Setup
    Setups.Full.valid(setup_from_schema)

    # Test

    sm = SettingsManager()
    assert get_package_file(str(sm.package_file), str(sm.schemas_file))
    assert get_package_file(Path(sm.package_file), Path(sm.schemas_file))

    with open(sm.schemas_file) as f:
        schemas_data = json.load(f)
    assert get_package_file(Path(sm.package_file), schemas_data)
