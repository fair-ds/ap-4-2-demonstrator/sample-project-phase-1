import os
import pytest

from src.FAIR_DS_DataQualityAssurance.settings import SettingsManager
from .util import gen_dummy_data, gen_schema, create_setup_from_schemas_file


@pytest.fixture(autouse=True)
def t(tmpdir):
    os.chdir(str(tmpdir))
    SettingsManager.clear()

    settings = SettingsManager()

    settings.data_folder.mkdir(parents=True, exist_ok=True)
    settings.schema_folder.mkdir(parents=True, exist_ok=True)
    settings.out_folder.mkdir(parents=True, exist_ok=True)
    settings.log_folder.mkdir(parents=True, exist_ok=True)


@pytest.fixture(scope='session')
def get_data():
    yield gen_dummy_data


@pytest.fixture(scope='session')
def get_schema():
    yield gen_schema


@pytest.fixture(scope='session')
def setup_from_schema():
    yield create_setup_from_schemas_file
