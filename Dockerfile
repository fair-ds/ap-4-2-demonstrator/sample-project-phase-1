# This file is a template, and might need editing before it works on your project.
FROM python:3.9

WORKDIR /usr/src/app

RUN pip install pandas geopandas scipy matplotlib frictionless pytest --no-cache-dir
RUN pip install git+https://jonhartm:nJyExD56oxxxizejK2PE@git.rwth-aachen.de/jonathan.a.hartman1/junit-xml-gen.git
RUN pip install git+https://jonhartm:FkSFkEm9DxQd_ZRCCdxv@git.rwth-aachen.de/fair-ds/ap-4-2-demonstrator/sample-project-phase-1.git@dev-jh

ADD /src/FAIR_DS_DataQualityAssurance/static pages/static

RUN pip list

# For some other command
CMD ["python", "-V"]
